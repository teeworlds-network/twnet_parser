def cmp_bytes_as_bin(got: bytes, expected: bytes) -> None:
    g_bin = [format(int(x), '#010b') for x in got]
    e_bin = [format(int(x), '#010b') for x in expected]
    assert g_bin == e_bin

