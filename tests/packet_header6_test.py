from twnet_parser.packet import \
    PacketHeaderParser6
from twnet_parser.packet import parse6
from twnet_parser.packet6 import PacketFlags6, PacketHeader6

def test_parse_flags() -> None:
    parser = PacketHeaderParser6()
    flags: PacketFlags6 = parser.parse_flags6(b'\x10\x00\x00')
    assert flags.token is False
    assert flags.control is True
    assert flags.connless is False
    assert flags.resend is False
    assert flags.compression is False

    flags = parser.parse_flags6(b'\x08\x01\x01\xCE\x2D\x2D\x65\x40\x01\x02\x1D')
    assert flags.token is True
    assert flags.control is False
    assert flags.connless is False
    assert flags.resend is False
    assert flags.compression is False

def test_packet_header_no_token_unpack() -> None:
    parser = PacketHeaderParser6()
    header: PacketHeader6 = parser.parse_header(b'\x10\x00\x00')

    assert header.ack == 0
    assert header.num_chunks == 0

    assert header.flags.control is True
    assert header.flags.resend is False
    assert header.flags.compression is False
    assert header.flags.connless is False

def test_parse_065_close():
    packet = parse6(b'\x10\x12\x00\x04\x9a\xcb\x09\xc9') # 0.6.5 close

    # TODO: the test name says 065 why does this say 0.6.4 who is right?
    assert packet.version == '0.6.4'

    # ack 18 is verified with libtw2
    assert packet.header.ack == 18
    # TODO: uncomment when 0.6 is done
    # assert packet.header.token == b'\x9a\xcb\x09\xc9'
    assert packet.header.num_chunks == 0

    assert packet.header.flags.control == True
    assert packet.header.flags.resend == False
    assert packet.header.flags.compression == False
    assert packet.header.flags.connless == False

    assert packet.messages[0].message_name == 'close'
    assert len(packet.messages) == 1

