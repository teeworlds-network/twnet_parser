from typing import cast

from twnet_parser.messages6.game.cl_change_info import MsgClChangeInfo
from twnet_parser.packet import TwPacket7, TwPacket6, NetMessage

def test_empty_packet7() -> None:
    packet = dict(TwPacket7())
    assert packet == {
        'header': {
            'ack': 0,
            'flags': [],
            'num_chunks': None,
            'token': b'\xff\xff\xff\xff'
        },
        'payload_decompressed': b'',
        'payload_raw': b'',
        'version': '0.7',
        'messages': []
    }

def test_change_info6() -> None:
    change_info = dict(MsgClChangeInfo())
    assert change_info['message_type'] == 'game'
    assert change_info['message_name'] == 'cl_change_info'
    assert change_info['system_message'] == False
    assert change_info['message_id'] == 21
    assert change_info['header'] == {'flags': [], 'seq': -1, 'size': None, 'version': '0.6'}

    assert change_info['name'] == 'default'
    assert change_info['clan'] == 'default'
    assert change_info['country'] == 0
    assert change_info['skin'] == 'default'
    assert change_info['use_custom_color'] == False
    assert change_info['color_body'] == 0
    assert change_info['color_feet'] == 0

def test_change_info_packet6() -> None:
    packet = TwPacket6(version='0.6')
    packet.messages.append(cast(NetMessage, MsgClChangeInfo()))
    packet = dict(packet)
    assert packet == {
        'header': {
            'ack': 0,
            'flags': [],
            'num_chunks': None,
            'token': b'\xff\xff\xff\xff'
        },
        'payload_decompressed': b'',
        'payload_raw': b'',
        'version': '0.6',
        'is_ddnet': False,
        'messages': [
            {
                'clan': 'default',
                'color_body': 0,
                'color_feet': 0,
                'country': 0,
                'header': {
                    'flags': [],
                    'seq': -1,
                    'size': None,
                    'version': '0.6'
                },
                'message_id': 21,
                'message_name': 'cl_change_info',
                'message_type': 'game',
                'name': 'default',
                'skin': 'default',
                'system_message': False,
                'use_custom_color': False
            }
        ]
    }

