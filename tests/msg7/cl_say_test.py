from twnet_parser.messages7.game.cl_say import MsgClSay

def test_cl_say_repack()-> None:
    # chunk header: 40 14 05 30
    data = \
        b'\x01\x40\x61\x61\x61\x61\x61' \
        b'\x61\x61\x61\x61\x61\x61\x61' \
        b'\x61\x61\x61\x61\x00'

    say = MsgClSay()
    say.unpack(data)

    assert say.message_name == 'cl_say'
    assert say.system_message is False
    assert say.message_id == 24
    assert say.mode == 1
    assert say.target == -1
    assert say.message == 'aaaaaaaaaaaaaaaa'

    repack = say.pack()

    assert repack == data

def test_cl_say_should_ignore_unwated_bytes_at_the_end()-> None:
    # chunk header: 40 14 05 30
    data = \
        b'\x01\x40\x61\x61\x61\x61\x61' \
        b'\x61\x61\x61\x61\x61\x61\x61' \
        b'\x61\x61\x61\x61\x00this should be ignored'

    say = MsgClSay()
    say.unpack(data)

    assert say.message_name == 'cl_say'
    assert say.system_message is False
    assert say.message_id == 24
    assert say.mode == 1
    assert say.target == -1
    assert say.message == 'aaaaaaaaaaaaaaaa'

def test_cl_say_target_default()-> None:
    say = MsgClSay()
    say.message = 'aaaaaaaaaaaaaaaa'
    say.mode = 1 # TODO: chat all should be default too

    data = \
        b'\x01\x40\x61\x61\x61\x61\x61' \
        b'\x61\x61\x61\x61\x61\x61\x61' \
        b'\x61\x61\x61\x61\x00'

    assert data == say.pack()

    assert say.message_name == 'cl_say'
    assert say.system_message is False
    assert say.message_id == 24
    assert say.mode == 1
    assert say.target == -1
    assert say.message == 'aaaaaaaaaaaaaaaa'

def test_cl_say_header_should_not_cache()-> None:
    msg_a = MsgClSay()
    msg_a.header.size = 10

    msg_b = MsgClSay()
    assert msg_b.header.size != 10

