from twnet_parser.messages7.game.sv_game_info import MsgSvGameInfo

def test_game_info_repack()-> None:
    # chunk header: 0x40, 0x06, 0x09
    data = bytes([
         0x00, 0x14, 0x00, 0x00, 0x01,
    ])


    info = MsgSvGameInfo()
    info.unpack(data)

    assert info.message_name == 'sv_game_info'
    assert info.system_message is False
    assert info.game_flags == 0
    assert info.score_limit == 20
    assert info.time_limit == 0
    assert info.match_num == 0
    assert info.match_current == 1

    repack = info.pack()

    assert repack == data

