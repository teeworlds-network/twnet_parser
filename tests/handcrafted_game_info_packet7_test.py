from typing import cast
from twnet_parser.messages7.game.sv_game_info import MsgSvGameInfo
from twnet_parser.packet import parse7

def test_handcrafted_packet_with_msg_game_info()-> None:
    # based on a real packet
    # which was compressed and had 2 more messages
    # the test case is now using a hand patched version that
    # has no huffman compression
    # for simplicity
    packet = parse7(bytes([
         # packet header
         0x00, 0x04, 0x02, 0x01, 0x02, 0x03, 0x04,
         # chunk header
         0x40, 0x06, 0x09,
         # system false message id 19
         0x26,
         # game info
         0x00, 0x14, 0x00, 0x00, 0x01,
    ]))

    assert packet.version == '0.7'
    assert len(packet.messages) == 1

    msg = cast(MsgSvGameInfo, packet.messages[0])
    assert msg.header.seq == 9

    assert msg.message_id == 19
    assert msg.message_name == 'sv_game_info'

