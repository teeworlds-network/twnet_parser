from twnet_parser.packet import parse6, TwPacket6
from twnet_parser.messages6.system.info import MsgInfo
from twnet_parser.packet6 import PacketHeader6

def test_unpack_info():
    """
    Sent by a vanilla 0.6.5 client
    copied from a real traffic capture

    https://chillerdragon.github.io/teeworlds-protocol/06/system_messages.html#NETMSG_INFO
    """
    data = \
        b'\x08\x00\x01' \
        b'\x9c\x6b\xcb\xda' \
        b'\x41\x07\x01\x03\x30\x2e\x36' \
        b'\x20\x36\x32\x36\x66\x63\x65' \
        b'\x39\x61\x37\x37\x38\x64\x66' \
        b'\x34\x64\x34\x00\x00'

    packet = parse6(data)

    assert packet.version == '0.6'
    assert packet.header.ack == 0
    assert packet.header.num_chunks == 1
    assert packet.header.token == b'\x9c\x6b\xcb\xda'
    assert packet.header.flags.token is True
    assert packet.header.flags.control is False
    assert packet.header.flags.connless is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.compression is False

    assert len(packet.messages) == 1

    msg: MsgInfo = packet.messages[0]

    assert msg.system_message is True
    assert msg.message_name == 'info'
    assert msg.version == '0.6 626fce9a778df4d4'
    assert msg.password == ''

    repack = packet.pack()
    assert repack == data

def test_pack_info():
    packet: TwPacket6 = TwPacket6()
    packet.header = PacketHeader6()
    packet.header.token = b'\xfa\xfa\xfa\xfa'
    msg: MsgInfo = MsgInfo()
    msg.version = '0.6 626fce9a778df4d4'
    msg.password = ''
    packet.messages.append(msg)
    data: bytes = packet.pack()

    packet2: TwPacket6 = parse6(data)
    assert packet2.version == '0.6'
    assert packet2.header.ack == 0
    assert packet2.header.num_chunks == 1
    assert packet2.header.token == b'\xfa\xfa\xfa\xfa'
    assert packet2.header.flags.token is True
    assert packet2.header.flags.control is False
    assert packet2.header.flags.connless is False
    assert packet2.header.flags.resend is False
    assert packet2.header.flags.compression is False
    assert len(packet.messages) == 1
    msg2: MsgInfo = packet2.messages[0]
    assert msg2.system_message is True
    assert msg2.message_name == 'info'
    assert msg2.version == '0.6 626fce9a778df4d4'
    assert msg2.password == ''

    assert packet2.pack() == data

