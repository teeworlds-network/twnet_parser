from typing import cast

from twnet_parser.messages6.game.cl_start_info import MsgClStartInfo
from twnet_parser.messages6.system.rcon_cmd import MsgRconCmd
from twnet_parser.packet import parse6

def test_unpack_ddnet_start_info_and_rcon() -> None:
    """
    Sent by a ddnet 16.2.1 client using a 0.6 connection
    copied from a real traffic capture

    https://chillerdragon.github.io/teeworlds-protocol/06/game_messages.html#NETMSGTYPE_CL_STARTINFO
    https://chillerdragon.github.io/teeworlds-protocol/06/system_messages.html#NETMSG_RCON_CMD
    """

    data = \
        b'\x00\x06\x02\x42\x0d\x05\x28\x43\x68\x69\x6c\x6c\x65\x72\x44\x72' \
        b'\x61\x67\x6f\x6e\x00\x7c\x2a\x4b\x6f\x47\x2a\x7c\x00\x80\x01\x67' \
        b'\x72\x65\x65\x6e\x73\x77\x61\x72\x64\x00\x00\x87\xc5\x8d\x0e\x8e' \
        b'\xab\x9e\x02\x40\x0c\x06\x23\x63\x72\x61\x73\x68\x6d\x65\x70\x6c' \
        b'\x78\x00\x3d\xe3\x94\x8d'
    #              ^
    #              ddnet token starts here

    packet = parse6(data)

    assert packet.version == '0.6'

    # should detect it as ddnet packet based on the 4 byte
    # security token at the end of the packet
    assert packet.is_ddnet is True
    assert packet.header.ack == 6
    assert packet.header.num_chunks == 2
    assert packet.header.flags.token is False
    assert packet.header.flags.control is False
    assert packet.header.flags.connless is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.compression is False

    # TODO: get ddnet token instead of 0.6.5 token
    # assert packet.header.token == b'\x3d\xe3\x94\x8d'

    assert len(packet.messages) == 2

    msg: MsgClStartInfo = cast(MsgClStartInfo, packet.messages[0])

    assert msg.header.flags.vital is True
    assert msg.header.flags.resend is False
    assert msg.header.size == 45
    assert msg.header.seq == 5

    assert msg.system_message is False
    assert msg.message_name == 'cl_start_info'
    assert msg.name == "ChillerDragon"
    assert msg.clan == "|*KoG*|"
    assert msg.country == 64
    assert msg.skin == "greensward"
    assert msg.use_custom_color is False
    assert msg.color_body == 14790983
    assert msg.color_feet == 2345678

    msg_rcon: MsgRconCmd = cast(MsgRconCmd, packet.messages[1])
    assert msg_rcon.system_message is True
    assert msg_rcon.cmd == "crashmeplx"

    repack = packet.pack()
    assert repack == data

