from typing import cast

from twnet_parser.packet import parse6, TwPacket6
from twnet_parser.messages6.game.cl_start_info import MsgClStartInfo
from twnet_parser.packet6 import PacketHeader6

def test_unpack_cl_start_info() -> None:
    """
    Sent by a vanilla 0.6.5 client
    copied from a real traffic capture

    https://chillerdragon.github.io/teeworlds-protocol/06/game_messages.html#NETMSGTYPE_CL_STARTINFO
    """
    data = \
        b'\x08\x03\x01' \
        b'\x9c\x6b\xcb\xda' \
        b'\x41' \
        b'\x0f\x03\x28\x6e\x61\x6d\x65\x6c\x65\x73' \
        b'\x73\x20\x74\x65\x65\x00\x00\x40\x64\x65\x66\x61\x75\x6c\x74\x00' \
        b'\x00\x80\xfe\x07\x80\xfe\x07'

    packet = parse6(data)

    assert packet.version == '0.6'
    assert packet.header.ack == 3
    assert packet.header.num_chunks == 1
    assert packet.header.flags.token is True
    assert packet.header.flags.control is False
    assert packet.header.flags.connless is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.compression is False

    assert len(packet.messages) == 1

    msg: MsgClStartInfo = cast(MsgClStartInfo, packet.messages[0])

    assert msg.system_message is False
    assert msg.message_name == 'cl_start_info'
    assert msg.name == "nameless tee"
    assert msg.clan == ""
    assert msg.country == -1
    assert msg.skin == "default"
    assert msg.use_custom_color is False
    assert msg.color_body == 65408
    assert msg.color_feet == 65408

    repack = packet.pack()
    assert repack == data

def test_pack_start_info():
    packet: TwPacket6 = TwPacket6()
    packet.header = PacketHeader6()
    packet.header.ack = 3
    packet.header.token = b'\x9c\x6b\xcb\xda'
    msg: MsgClStartInfo = MsgClStartInfo()
    msg.name = "nameless tee"
    msg.clan = ""
    msg.country = -1
    msg.skin = "default"
    msg.use_custom_color = False
    msg.color_body = 65408
    msg.color_feet = 65408
    packet.messages.append(msg)
    data: bytes = packet.pack()

    packet2: TwPacket6 = parse6(data)
    assert packet2.version == '0.6'
    assert packet2.header.ack == 3
    assert packet2.header.num_chunks == 1
    assert packet2.header.flags.token is True
    assert packet2.header.flags.control is False
    assert packet2.header.flags.connless is False
    assert packet2.header.flags.resend is False
    assert packet2.header.flags.compression is False
    assert len(packet2.messages) == 1
    msg2: MsgClStartInfo = cast(MsgClStartInfo, packet2.messages[0])
    assert msg2.system_message is False
    assert msg2.message_name == 'cl_start_info'
    assert msg2.name == "nameless tee"
    assert msg2.clan == ""
    assert msg2.country == -1
    assert msg2.skin == "default"
    assert msg2.use_custom_color is False
    assert msg2.color_body == 65408
    assert msg2.color_feet == 65408

    assert packet2.pack() == data

def test_pack_start_info_no_token():
    packet: TwPacket6 = TwPacket6()
    packet.header = PacketHeader6()
    packet.header.ack = 3
    packet.header.flags.token = False
    packet.header.token = b''
    msg: MsgClStartInfo = MsgClStartInfo()
    msg.name = "nameless tee"
    msg.clan = ""
    msg.country = -1
    msg.skin = "default"
    msg.use_custom_color = False
    msg.color_body = 65408
    msg.color_feet = 65408
    packet.messages.append(msg)
    data: bytes = packet.pack()

    packet2: TwPacket6 = parse6(data)
    assert packet2.version == '0.6'
    assert packet2.header.ack == 3
    assert packet2.header.num_chunks == 1
    assert packet2.header.flags.token is False
    assert packet2.header.flags.control is False
    assert packet2.header.flags.connless is False
    assert packet2.header.flags.resend is False
    assert packet2.header.flags.compression is False
    assert len(packet2.messages) == 1
    msg2: MsgClStartInfo = cast(MsgClStartInfo, packet2.messages[0])
    assert msg2.system_message is False
    assert msg2.message_name == 'cl_start_info'
    assert msg2.name == "nameless tee"
    assert msg2.clan == ""
    assert msg2.country == -1
    assert msg2.skin == "default"
    assert msg2.use_custom_color is False
    assert msg2.color_body == 65408
    assert msg2.color_feet == 65408

    assert packet2.pack() == data
