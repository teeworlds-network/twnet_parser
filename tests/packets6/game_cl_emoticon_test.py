from twnet_parser.packet import parse6, TwPacket6
from twnet_parser.messages6.game.cl_emoticon import MsgClEmoticon
from twnet_parser.messages6.system.input import MsgInput

from typing import cast

# def test_real_ddnet_cl_emote_and_input6() -> None:
#     """
#     This is from a traffic dump of a ddnet 0.6
#     client talking to a ddnet server
# 
#     the last 4 bytes seem to be the ddnet token
#     twnet_parser does not support the ddnet protocol yet
#     """
#     data = b'\x80\x13' \
#         b'\x02\x4a\x42\xbb\x8b\x6e\x28\x18\xf1\x92\xe5\x82\x2c\x6b\x99\x36' \
#         b'\x1a\xf1\x78\x3e\x54\x0d\x67\x52\xaf\xb8\x01' \
# 
#     packet = parse6(data)
# 
#     assert packet.header.num_chunks == 2
#     assert packet.header.ack == 19
# 
#     assert packet.header.flags.resend is False
#     assert packet.header.flags.connless is False
#     assert packet.header.flags.control is False
#     assert packet.header.flags.compression is True
# 
#     assert len(packet.messages) == 2
# 
#     emote: MsgClEmoticon = cast(MsgClEmoticon, packet.messages[0])
#     assert emote.message_name == 'cl_emoticon'
#     assert emote.emoticon == 7
# 
#     inp: MsgInput = cast(MsgInput, packet.messages[1])
#     assert inp.message_name == 'input'
#     assert inp.ack_snapshot == 2556
#     assert inp.intended_tick == 2557
#     assert inp.input_size == 40
# 
#     assert inp.input.direction == 0
#     assert inp.input.target_x == 394
#     assert inp.input.target_y == 68
#     assert inp.input.hook == 0
#     assert inp.input.fire == 0
#     assert inp.input.jump == 0
#     assert inp.input.player_flags == 1
#     assert inp.input.wanted_weapon == 0
#     assert inp.input.next_weapon == 0
#     assert inp.input.prev_weapon == 0

def test_repack_real_ddnet_cl_emote_input6() -> None:
    packet: TwPacket6 = TwPacket6(version = '0.6')
    packet.header.ack = 19
    # packet.header.flags.compression = True # TODO: fix compressing packets
    emote = MsgClEmoticon()
    emote.emoticon = 7
    packet.messages.append(emote)

    inp = MsgInput()
    inp.ack_snapshot = 2556
    inp.intended_tick = 2557
    inp.input_size = 40
    inp.input.direction = 0
    inp.input.target_x = 394
    inp.input.target_y = 68
    inp.input.hook = 0
    inp.input.fire = 0
    inp.input.jump = 0
    inp.input.player_flags = 1
    inp.input.wanted_weapon = 0
    inp.input.next_weapon = 0
    inp.input.prev_weapon = 0
    packet.messages.append(inp)

    handcrafted: bytes = packet.pack()
    print(packet.header.flags)

    packet2: TwPacket6 = parse6(handcrafted)

    assert packet2.version == '0.6'
    assert packet2.header.num_chunks == 2
    assert packet2.header.ack == 19

    assert packet2.header.flags.resend is False
    assert packet2.header.flags.connless is False
    assert packet2.header.flags.control is False
    # assert packet2.header.flags.compression is True

    assert len(packet2.messages) == 2

    emote2: MsgClEmoticon = cast(MsgClEmoticon, packet2.messages[0])
    assert emote2.message_name == 'cl_emoticon'
    assert emote2.emoticon == 7

    inp2: MsgInput = cast(MsgInput, packet2.messages[1])
    assert inp2.message_name == 'input'
    assert inp2.ack_snapshot == 2556
    assert inp2.intended_tick == 2557
    assert inp2.input_size == 40

    assert inp2.input.direction == 0
    assert inp2.input.target_x == 394
    assert inp2.input.target_y == 68
    assert inp2.input.hook == 0
    assert inp2.input.fire == 0
    assert inp2.input.jump == 0
    assert inp2.input.player_flags == 1
    assert inp2.input.wanted_weapon == 0
    assert inp2.input.next_weapon == 0
    assert inp2.input.prev_weapon == 0

def test_pack_cl_emoticon6() -> None:
    packet: TwPacket6 = TwPacket6(version = '0.6')
    emote = MsgClEmoticon(
            emoticon = 7
    )
    packet.messages.append(emote)

    data = packet.pack()
    assert data == b'\x00\x00\x01\x00\x02.\x07'

    packet2: TwPacket6 = parse6(data)
    assert packet2.version == '0.6'

    assert packet2.header.num_chunks == 1
    assert packet2.header.ack == 0

    assert packet2.header.flags.control is False
    assert packet2.header.flags.compression is False

    assert len(packet2.messages) == 1

    msg: MsgClEmoticon = cast(MsgClEmoticon, packet2.messages[0])

    assert msg.message_name == 'cl_emoticon'

    assert msg.emoticon == 7

    assert msg.header.flags.resend is False
    assert msg.header.flags.vital is False # TODO: this should be True by default
    assert msg.header.size == 2
    assert msg.header.seq == -1 # TODO: that should not be negative by default

