from twnet_parser.packet import parse6
from twnet_parser.messages6.connless.info import MsgInfo

def test_unpack_info():
    """
    Sent by a vanilla 0.6.5 server
    copied from a real traffic capture
    """
    data = \
        b'\xff\xff' \
        b'\xff\xff\xff\xff\xff\xff\xff\xff\x69\x6e\x66\x33\x32\x00\x30\x2e' \
        b'\x36\x2e\x35\x00\x75\x6e\x6e\x61\x6d\x65\x64\x20\x73\x65\x72\x76' \
        b'\x65\x72\x00\x42\x6c\x6d\x61\x70\x43\x68\x69\x6c\x6c\x00\x44\x4d' \
        b'\x00\x30\x00\x31\x00\x38\x00\x31\x00\x38\x00\x6e\x61\x6d\x65\x6c' \
        b'\x65\x73\x73\x20\x74\x65\x65\x00\x00\x2d\x31\x00\x30\x00\x31\x00'

    packet = parse6(data)

    assert packet.version == '0.6'
    assert packet.header.ack == 0
    assert packet.header.num_chunks is None
    assert packet.header.token == b'\xff\xff\xff\xff'
    assert packet.header.flags.token is False
    assert packet.header.flags.control is False
    assert packet.header.flags.connless is True
    assert packet.header.flags.resend is False
    assert packet.header.flags.compression is False

    assert len(packet.messages) == 1

    msg: MsgInfo = packet.messages[0]

    assert msg.message_name == 'connless.info'
    assert msg.token == 2
    assert msg.version == '0.6.5'
    assert msg.name == "unnamed server"
    assert msg.map == "BlmapChill"
    assert msg.game_type == "DM"
    assert msg.flags == 0
    assert msg.num_players == 1
    assert msg.max_players == 8
    assert msg.num_clients == 1
    assert msg.max_clients == 8

    repack = packet.pack()
    assert repack == data



