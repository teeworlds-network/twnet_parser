from twnet_parser.packet import parse6
from twnet_parser.messages6.control.ack_accept_connect import CtrlAckAcceptConnect
from twnet_parser.packet6 import TwPacket6

def test_ctrl_ack_accept_connection():
    """
    This is the 2nd packet sent by the CLIENT
    the data is from a real traffic dump of a ddnet client

    https://chillerdragon.github.io/teeworlds-protocol/06/ctrl_messages.html#NET_CTRLMSG_ACCEPT
    """
    data = b'\x10\x00\x00\x03\x3d\xe3\x94\x8d'

    packet = parse6(data)

    assert packet.version == '0.6.4'
    assert packet.header.ack == 0
    assert packet.header.num_chunks == 0
    assert packet.header.flags.token is False
    assert packet.header.flags.control is True
    assert packet.header.flags.connless is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.compression is False

    assert packet.is_ddnet is True
    assert packet.header.token == b'\x3d\xe3\x94\x8d'

    assert len(packet.messages) == 1

    msg = packet.messages[0]

    assert msg.message_name == 'ack_accept_connect'

    repack = packet.pack()
    assert repack == data

    # # TODO: packing ddnet packets should work
    # packet2 = TwPacket6()
    # packet2.header.token = b'\x3d\xe3\x94\x8d'
    # packet2.messages.append(CtrlAckAcceptConnect())
    # assert packet2.pack() == data
