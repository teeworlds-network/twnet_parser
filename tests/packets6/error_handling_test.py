import pytest

from twnet_parser.packet import parse6

def test_unpack_system_error():
    """
    Some bugged attempt at sending 0.6.4 ddnet sys info as client
    """
    data = b'\x00\x00\x01' \
        b'\x40\x28\x01\xde\x30\x2e\x36' \
        b'\x20\x36\x32\x36\x66\x63\x65' \
        b'\x39\x61\x37\x37\x38\x64\x66' \
        b'\x34\x64\x34\x00'

    expected_error_msg = 'Error: unknown system message id=-1552 data=2e 36 20 36 32 36 66 63 65 39 61 37 37 38 64 66 34 64 34 00'
    with pytest.raises(ValueError, match=expected_error_msg):
        parse6(data)

