from twnet_parser.packet import parse6
from twnet_parser.messages6.game.sv_chat import MsgSvChat
from twnet_parser.messages6.system.snap_single import MsgSnapSingle

def test_game_sv_chat():
    """
    Sent by a vanilla 0.6.5 server
    copied from a real traffic capture
    """
    data = b'\x08\x04' \
		b'\x02\x9c\x6b\xcb\xda\x42\x0e\x07\x06\x00\x40\x27\x6e\x61\x6d\x65' \
		b'\x6c\x65\x73\x73\x20\x74\x65\x65\x27\x20\x65\x6e\x74\x65\x72\x65' \
		b'\x64\x20\x61\x6e\x64\x20\x6a\x6f\x69\x6e\x65\x64\x20\x74\x68\x65' \
		b'\x20\x67\x61\x6d\x65\x00\x06\x0b\x0f\x9a\x51\x9b\x51\xb3\x92\xad' \
		b'\xb9\x0b\x9f\x01\x00\x03\x00\x06\x00\x00\x00\x00\x00\x14\x00\x00' \
		b'\x01\x0b\x00\xda\xc8\xf0\x91\x02\xcc\xb0\xd0\xb1\x02\xda\xe8\xd8' \
		b'\xf0\x0b\xff\xff\xfb\xf7\x0f\xff\xfd\xfb\xf7\x0f\xff\xfd\xfb\xf7' \
		b'\x0f\xff\xff\xfb\xf7\x0f\x40\xde\xe4\xd0\xb1\x03\xff\xad\x98\xa1' \
		b'\x01\xff\xfd\xfb\xf7\x0f\xff\xfd\xfb\xf7\x0f\xff\xfd\xfb\xf7\x0f' \
		b'\xff\xff\xfb\xf7\x0f\x00\x80\xfe\x07\x80\xfe\x07\x0a\x00\x01\x00' \
		b'\x00\x00\x00'


    packet = parse6(data)

    assert packet.version == '0.6'
    assert packet.header.ack == 4
    assert packet.header.num_chunks == 2
    assert packet.header.flags.token is True
    assert packet.header.flags.control is False
    assert packet.header.flags.connless is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.compression is False

    assert len(packet.messages) == 2

    msg: MsgSvChat = packet.messages[0]

    assert msg.system_message is False
    assert msg.message_name == 'sv_chat'
    assert msg.client_id == -1
    assert msg.message == "'nameless tee' entered and joined the game"

    msg2: MsgSnapSingle = packet.messages[1]
    assert msg2.system_message is True
    assert msg2.message_name == 'snap_single'
    assert msg2.tick == 5210
    assert msg2.delta_tick == 5211
    assert msg2.crc == 1536533683
    assert msg2.data_size == 95
    # assert msg2.data == b''

    repack = packet.pack()
    assert repack == data

