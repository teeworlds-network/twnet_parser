from twnet_parser.uuid_manager import UuidManager

def test_register_name_should_not_crash() -> None:
    manager = UuidManager()
    manager.register_name(0, 'foo')

def test_register_name_get_name() -> None:
    manager = UuidManager()
    manager.register_name(0, 'foo')

    uuid = manager.get_uuid_by_type(0)
    assert uuid.name == 'foo'

def test_calculate_uuid() -> None:
    manager = UuidManager()
    cap = manager.calculate_uuid('capabilities@ddnet.tw')
    # verified with the ddnet uuid tool and by looking at the network traffic
    # f621a5a1-f585-3775-8e73-41beee79f2b2
    assert cap == b'\xf6\x21\xa5\xa1\xf5\x85\x37\x75\x8e\x73\x41\xbe\xee\x79\xf2\xb2'

def test_register_calc_uuid() -> None:
    manager = UuidManager()
    manager.register_name(0, 'foo')
    uuid = manager.get_uuid_by_type(0)

    # 09ea064a-7bb3-3557-bf2f-7ae3db95106a
    assert uuid.name == 'foo'
    assert uuid.uuid == b'\x09\xea\x06\x4a\x7b\xb3\x35\x57\xbf\x2f\x7a\xe3\xdb\x95\x10\x6a'

def test_register_calc_multiple_uuids() -> None:
    manager = UuidManager()
    manager.register_name(0, 'foo')
    manager.register_name(1, 'bar')
    foo = manager.get_uuid_by_type(0)
    bar = manager.get_uuid_by_type(1)

    # 09ea064a-7bb3-3557-bf2f-7ae3db95106a
    assert foo.type_id == 0
    assert foo.name == 'foo'
    assert foo.uuid == b'\x09\xea\x06\x4a\x7b\xb3\x35\x57\xbf\x2f\x7a\xe3\xdb\x95\x10\x6a'

    # 71111349-4e74-3e36-81ff-fe712097b4cc
    assert bar.type_id == 1
    assert bar.name == 'bar'
    assert bar.uuid == b'\x71\x11\x13\x49\x4e\x74\x3e\x36\x81\xff\xfe\x71\x20\x97\xb4\xcc'

def test_register_conflicting_types() -> None:
    # TODO: this should crash
    manager = UuidManager()
    manager.register_name(0, 'foo')
    manager.register_name(0, 'bar')

