from twnet_parser.packet import \
    ChunkHeaderParser
from twnet_parser.chunk_header import \
    ChunkHeader

def test_chunk_header_unpack_vital_seq129_siz8() -> None:
    # from broken teeworlds-asmr client
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x40\x28\x01')
    # TODO: ddnet packs 0x40, 0x28, 0x81 so the last byte is different
	#       CNetChunkHeader Header;
	#       Header.m_Flags = NET_CHUNKFLAG_VITAL;
	#       Header.m_Size = 8;
	#       Header.m_Sequence = 129;
	#       unsigned char aData[8];
	#       unsigned char *pData = &aData[0];
	#       Header.Pack(pData);
	#       EXPECT_EQ(pData[0], 0x40);
	#       EXPECT_EQ(pData[1], 0x28);
	#       EXPECT_EQ(pData[2], 0x81);

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 8
    assert header.seq == 129

def test_chunk_header_unpack_vital_seq3_siz23() -> None:
    # from ddnet traffic dump verified by libtw2 dissector
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x41\x07\x03')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 23
    assert header.seq == 3

def test_chunk_header_unpack_vital_seq65_siz6() -> None:
    # unverified made up values
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x40\x16\x01')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 6
    assert header.seq == 65

def test_chunk_header_unpack_vital_seqff_siz6() -> None:
    # unverified made up values
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x40\x16\xff')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 6
    assert header.seq == 255

def test_chunk_header_unpack_vital_seqff_siz0() -> None:
    # unverified made up values
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x40\x00\xff')
    # i added a debug print in ddnet and it wants     x30  as second byte?
    # conceptually i do not understand why because 255 is already fully
    # contained in xff
    # see the related research pull request here https://github.com/ddnet/ddnet/pull/9417
    # it does seem like we do not implement the reference implementation correctly
    # but i also have the feeling it might not matter because
    # the reference implementation makes no sense to me

    # TODO: remove the above comment and make sure we do the right thing here
    #       this depends on either building a working 0.6 client and verify it
    #       never messes up high sequence numbers
    #       or wait for the ddnet pr to give some clarity

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 0
    assert header.seq == 255

def test_chunk_header_unpack_vital_siz18_seq1() -> None:
    # verified with dbg print in ddnet client
    # https://github.com/ddnet/ddnet/blob/b52f1a41a5890fca6da3a877221f7dce69eec60c/src/engine/shared/network.cpp#L375
    # dbg_msg("unpack", "%s      fgs=%d siz=%d seq=%d", aHex, m_Flags, m_Size, m_Sequence);
    # https://github.com/ChillerDragon/GistsPublic/blob/0b60c6852bb3b43d73efad59a41c43de772172a8/ddnet6_chunk_header.txt
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x41\x02\x01')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 18
    assert header.seq == 1

def test_chunk_header_unpack_vital_siz19_seq2() -> None:
    # verified with dbg print in ddnet client
    # https://github.com/ddnet/ddnet/blob/b52f1a41a5890fca6da3a877221f7dce69eec60c/src/engine/shared/network.cpp#L375
    # dbg_msg("unpack", "%s      fgs=%d siz=%d seq=%d", aHex, m_Flags, m_Size, m_Sequence);
    # https://github.com/ChillerDragon/GistsPublic/blob/0b60c6852bb3b43d73efad59a41c43de772172a8/ddnet6_chunk_header.txt
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x41\x03\x02')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 19
    assert header.seq == 2

def test_chunk_header_unpack_vital_siz70_seq3() -> None:
    # verified with dbg print in ddnet client
    # https://github.com/ddnet/ddnet/blob/b52f1a41a5890fca6da3a877221f7dce69eec60c/src/engine/shared/network.cpp#L375
    # dbg_msg("unpack", "%s      fgs=%d siz=%d seq=%d", aHex, m_Flags, m_Size, m_Sequence);
    # https://github.com/ChillerDragon/GistsPublic/blob/0b60c6852bb3b43d73efad59a41c43de772172a8/ddnet6_chunk_header.txt
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x44\x06\x03')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 70
    assert header.seq == 3

def test_chunk_header_unpack_vital_siz17_seq204() -> None:
    # verified with dbg print in ddnet client
    # https://github.com/ddnet/ddnet/blob/b52f1a41a5890fca6da3a877221f7dce69eec60c/src/engine/shared/network.cpp#L375
    # dbg_msg("unpack", "%s      fgs=%d siz=%d seq=%d", aHex, m_Flags, m_Size, m_Sequence);
    # https://github.com/ChillerDragon/GistsPublic/blob/0b60c6852bb3b43d73efad59a41c43de772172a8/ddnet6_chunk_header.txt
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x41\x31\xCC')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 17
    assert header.seq == 204

def test_chunk_header_unpack_vital_seqff_siz49() -> None:
    # verified with libtw2 dissector
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x43\x31\xff')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 49
    assert header.seq == 255

def test_chunk_header_unpack_bogus_vital() -> None:
    # this is a non sense header
    # but the values are verified with the C++
    # implementation
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x40\x10\x0a')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 0
    assert header.seq == 74

def test_chunk_header_unpack_vital_seq38() -> None:
    # this is a non sense header
    # but the values are verified with the C++
    # implementation
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x40\x02\x26')

    assert header.flags.resend is False
    assert header.flags.vital is True

    assert header.size == 2
    assert header.seq == 38

def test_chunk_header_unpack_bogus_no_flags_size107() -> None:
    # this is a non sense header
    # but the values are verified with the C++
    # implementation
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x06\x0b')

    assert header.flags.resend is False
    assert header.flags.vital is False

    assert header.size == 107
    assert header.seq == -1

def test_chunk_header_unpack_bogus_no_flags_size27() -> None:
    # this is a non sense header
    # but the values are verified with the C++
    # implementation
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\x01\x0b')

    assert header.flags.resend is False
    assert header.flags.vital is False

    assert header.size == 27
    assert header.seq == -1

def test_chunk_header_all_set() -> None:
    # this is a non sense header
    # but the values are verified with the C++
    # implementation
    parser = ChunkHeaderParser()
    header: ChunkHeader = parser.parse_header6(b'\xff\xff\xff')

    assert header.flags.resend is True
    assert header.flags.vital is True

    assert header.size == 1023
    assert header.seq == 1023

