from typing import cast
import textwrap

from twnet_parser.messages6.game.cl_change_info import MsgClChangeInfo
from twnet_parser.packet import TwPacket7, NetMessage

def test_change_info_packet() -> None:
    packet = TwPacket7()
    packet.messages.append(cast(NetMessage, MsgClChangeInfo()))
    packet = packet.to_json()
    expected = textwrap.dedent("""\
    {
      "version": "0.7",
      "payload_raw": "",
      "payload_decompressed": "",
      "header": {
        "flags": [],
        "ack": 0,
        "token": "ffffffff",
        "num_chunks": null
      },
      "messages": [
        {
          "message_type": "game",
          "message_name": "cl_change_info",
          "system_message": false,
          "message_id": 21,
          "header": {
            "version": "0.6",
            "flags": [],
            "size": null,
            "seq": -1
          },
          "name": "default",
          "clan": "default",
          "country": 0,
          "skin": "default",
          "use_custom_color": false,
          "color_body": 0,
          "color_feet": 0
        }
      ]
    }""")
    assert packet == expected
