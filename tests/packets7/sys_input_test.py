from twnet_parser.packet import parse7, TwPacket7
from twnet_parser.messages7.system.input import MsgInput

from typing import cast

def test_unpack_real_sys_info() -> None:
    """
    This packet is from a real traffic dump
    sent from a 0.7.5 client to a 0.7.5 server

    it was me holding walk left for most of the time
    so this input should be a left walking input

    The packet payload is compressed
    """
    data = \
        b'\x10\x0a\x01\xb5\x58\xe7\x4b' \
        b'\x4d\xe9\xc0\xe5' \
        b'\xa9\x90\xa7\x55' \
        b'\x09\xff\x7d\x52' \
        b'\xdc\x00'

    packet: TwPacket7 = parse7(data)
    assert packet.version == '0.7'

    assert packet.header.token == b'\xb5\x58\xe7\x4b'
    assert packet.header.num_chunks == 1
    assert packet.header.ack == 10

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is True
    assert packet.header.flags.connless is False

    assert len(packet.messages) == 1

    msg: MsgInput = cast(MsgInput, packet.messages[0])

    assert msg.header.flags.resend is False
    assert msg.header.flags.vital is False
    assert msg.header.size == 17

    assert msg.message_name == 'input'

    assert msg.ack_snapshot == 1650
    assert msg.intended_tick == 1654
    assert msg.input_size == 40

    # these tested values are not really validated
    # against another implementation
    # but they seem about right
    # i was walking left so dir -1 is OK
    # i did not touch the mouse so aim 1/0
    # seems okay since the server disallows 0/0
    # and sets it to 1/0
    # i was not hooking/firing/jumping and so on
    # since i just joined and held the a key
    assert msg.input.direction == -1
    assert msg.input.target_x == 1
    assert msg.input.target_y == 0
    assert msg.input.jump == 0
    assert msg.input.fire == 0
    assert msg.input.hook == 0
    assert msg.input.player_flags == 0
    assert msg.input.wanted_weapon == 0
    assert msg.input.next_weapon == 0
    assert msg.input.prev_weapon == 0

