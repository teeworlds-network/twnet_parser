from twnet_parser.packet import parse7

def test_parse_7_real_snap_single():
    packet = parse7(b'\x10\x04\x01\x58\xeb\x9a\xf4' \
        b'\x7d\x8d\x29\x93\xe8\x07\xf4\xd9\xc7\x9e' \
        b'\xad\x2d\xda\x8c\xf5\x35\x22\xac\xaf\xa3' \
        b'\x1f\xb4\x07\xe2\x4a\xc3\xfa\x3a\x9a\xd4' \
        b'\xbe\xbe\x1e\xef\x9f\xac\xb8\x01')

    assert packet.version == '0.7'

    assert packet.header.token == b'\x58\xeb\x9a\xf4'
    assert packet.header.num_chunks == 1
    assert packet.header.ack == 4

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is True
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is False

    assert packet.payload_raw == \
        b'}\x8d)\x93\xe8\x07\xf4\xd9\xc7\x9e\xad-' \
        b'\xda\x8c\xf55"\xac\xaf\xa3\x1f\xb4\x07' \
        b'\xe2J\xc3\xfa:\x9a\xd4\xbe\xbe\x1e\xef' \
        b'\x9f\xac\xb8\x01'
    assert packet.payload_decompressed == \
        b'\x006\x11\x90\x01\x91\x01\xa2\x9d\x04-' \
        b'\x00\x03\x00\x06\x00\x00\x01\x00\n\x00' \
        b'\x84\x01\xb0\xe6\x01\x91&\x00\x80\x02' \
        b'\x00\x00\x00@\x00\x00\xb0\xe6\x01\x90&' \
        b'\x00\x00\n\x00\n\x01\x00\x00\x00\x0b\x00' \
        b'\x08\x00\x00'

    assert len(packet.messages) == 1

    snap = packet.messages[0]

    assert snap.message_name == 'snap_single'
    assert snap.system_message is True

    assert snap.tick == 80
    assert snap.delta_tick == 81
    assert snap.crc == 34658
    assert snap.data_size == 45
    assert snap.data == \
        b'\x00\x03\x00\x06\x00\x00\x01\x00\n\x00\x84' \
        b'\x01\xb0\xe6\x01\x91&\x00\x80\x02\x00\x00' \
        b'\x00@\x00\x00\xb0\xe6\x01\x90&\x00\x00\n' \
        b'\x00\n\x01\x00\x00\x00\x0b\x00\x08\x00\x00'

    # TODO: check snap items when done

