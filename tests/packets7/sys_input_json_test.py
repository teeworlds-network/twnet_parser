from twnet_parser.packet import parse7, TwPacket7
from twnet_parser.messages7.system.input import MsgInput

from typing import cast
import textwrap

def test_unpack_real_sys_info_and_turn_into_json() -> None:
    """
    This packet is from a real traffic dump
    sent from a 0.7.5 client to a 0.7.5 server

    it was me holding walk left for most of the time
    so this input should be a left walking input

    The packet payload is compressed
    """
    data = \
        b'\x10\x0a\x01\xb5\x58\xe7\x4b' \
        b'\x4d\xe9\xb0\x84\x68\x84\x58' \
        b'\x95\xf0\x5f\xad\xb8\x01'

    packet: TwPacket7 = parse7(data)
    assert packet.version == '0.7'

    assert packet.header.token == b'\xb5\x58\xe7\x4b'
    assert packet.header.num_chunks == 1
    assert packet.header.ack == 10

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is True
    assert packet.header.flags.connless is False

    assert len(packet.messages) == 1

    msg: MsgInput = cast(MsgInput, packet.messages[0])

    assert msg.header.flags.resend is False
    assert msg.header.flags.vital is False
    assert msg.header.size == 17

    assert msg.message_name == 'input'

    assert msg.ack_snapshot == 1538
    assert msg.intended_tick == 1540
    assert msg.input_size == 40

    # these tested values are not really validated
    # against another implementation
    # but they seem about right
    # i was walking left so dir -1 is OK
    # i did not touch the mouse so aim 1/0
    # seems okay since the server disallows 0/0
    # and sets it to 1/0
    # i was not hooking/firing/jumping and so on
    # since i just joined and held the a key
    assert msg.input.direction == -1
    assert msg.input.target_x == 1
    assert msg.input.target_y == 0
    assert msg.input.jump == 0
    assert msg.input.fire == 0
    assert msg.input.hook == 0
    assert msg.input.player_flags == 0
    assert msg.input.wanted_weapon == 0
    assert msg.input.next_weapon == 0
    assert msg.input.prev_weapon == 0

    expected = textwrap.dedent("""\
    {
      "version": "0.7",
      "payload_raw": "4de9b08468845895f05fadb801",
      "payload_decompressed": "00112982188418284001000000000000000010",
      "header": {
        "flags": [
          "compression"
        ],
        "ack": 10,
        "token": "b558e74b",
        "num_chunks": 1
      },
      "messages": [
        {
          "message_type": "system",
          "message_name": "input",
          "system_message": true,
          "message_id": 20,
          "header": {
            "version": "0.7",
            "flags": [],
            "size": 17,
            "seq": -1
          },
          "ack_snapshot": 1538,
          "intended_tick": 1540,
          "input_size": 40,
          "input": {
            "direction": -1,
            "target_x": 1,
            "target_y": 0,
            "jump": false,
            "fire": 0,
            "hook": false,
            "player_flags": 0,
            "wanted_weapon": 0,
            "next_weapon": 0,
            "prev_weapon": 0
          }
        }
      ]
    }""")

    assert packet.to_json() == expected
