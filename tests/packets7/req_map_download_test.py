from typing import cast

from twnet_parser.packet import parse7, TwPacket7
from twnet_parser.messages7.system.request_map_data import MsgRequestMapData

def test_real_req_map_data() -> None:
    """
    this is from a traffic dump of a 0.7.5
    client talking to a 0.7.5 server

    the map request message is has no payload
    so it always ends in hex 2F

    which is just the system flag and the packed message id 23

    0x2F >> 1 # => 23

    https://chillerdragon.github.io/teeworlds-protocol/07/system_messages.html#NETMSG_REQUEST_MAP_DATA
    """
    data = b'\x00\x01\x01\xad\x8f\x5f\xde' \
            b'\x40\x01\x02' \
            b'\x2f'

    packet: TwPacket7 = parse7(data)
    assert packet.version == '0.7'
    assert packet.header.token == b'\xad\x8f\x5f\xde'
    assert packet.header.num_chunks == 1

    assert len(packet.messages) == 1

    req: MsgRequestMapData = cast(MsgRequestMapData, packet.messages[0])
    assert req.message_name == 'request_map_data'

def test_build_req_map_data() -> None:
    packet = TwPacket7()
    req = MsgRequestMapData()
    req.header.flags.vital = True
    packet.messages.append(req)

    handpacked: bytes = packet.pack()

    assert handpacked == b'\x00\x00\x01\xff\xff\xff\xff\x40\xc1\xff\x2f'

    packet2: TwPacket7 = parse7(handpacked)
    assert packet2.version == '0.7'
    assert packet2.header.token == b'\xff\xff\xff\xff'
    assert packet2.header.num_chunks == 1

    assert len(packet2.messages) == 1

    req: MsgRequestMapData = cast(MsgRequestMapData, packet2.messages[0])
    assert req.message_name == 'request_map_data'

