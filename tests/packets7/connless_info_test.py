from typing import cast

from twnet_parser.packet import parse7
from twnet_parser.messages7.connless.count import MsgCount

def test_count_connless7() -> None:
    data = b'\x21\xaa\xf5\x91\x59\xba\xf2\xfd\x31' \
        b'\xff\xff\xff\xff\x69\x6e\x66\x33\x93\x93\xf0\xe0\x0e' \
        b'\x30\x2e\x37\xe2\x86\x94\x30\x2e\x36\x2e\x34\x2c\x20\x31\x36\x2e' \
        b'\x39\x00\x43\x68\x69\x6c\x6c\x65\x72\x44\x72\x61\x67\x6f\x6e\x27' \
        b'\x73\x20\x43\x68\x69\x6c\x6c\x42\x6c\x6f\x63\x6b\x20\x53\x65\x72' \
        b'\x76\x65\x72\x20\x5b\x43\x68\x69\x6c\x6c\x69\x2e\x2a\x5d\x00\x00' \
        b'\x43\x68\x69\x6c\x6c\x42\x6c\x6f\x63\x6b\x35\x00\x44\x44\x72\x61' \
        b'\x63\x65\x4e\x65\x74\x77\x6f\x72\x6b\x00\x02\x01\x06\x80\x01\x06' \
        b'\x80\x01'

    packet = parse7(data)

    assert packet.version == '0.7'

    assert packet.header.token == b'\xaa\xf5\x91\x59'
    assert packet.header.response_token == b'\xba\xf2\xfd\x31'

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is True

    assert len(packet.messages) == 1

    msg: MsgCount = cast(MsgCount, packet.messages[0])

    assert msg.message_name == 'connless.info'
    assert msg.token == 1980630227
    assert msg.version == '0.7↔0.6.4, 16.9'
    assert msg.name == "ChillerDragon's ChillBlock Server [Chilli.*]"
    assert msg.hostname == ""
    assert msg.map == "ChillBlock5"
    assert msg.game_type == "DDraceNetwork"
    assert msg.flags == 2
    assert msg.skill_level == 1
    assert msg.num_players == 6
    assert msg.max_players == 64
    assert msg.num_clients == 6
    assert msg.max_clients == 64

    repack: bytes = packet.pack()

    assert repack == data

