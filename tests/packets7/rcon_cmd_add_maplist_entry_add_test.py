from twnet_parser.packet import parse7

def test_parse_7_real_rcon_login_response():
    packet = parse7(b'\x00\x05\x30\x4d\xcb\x93\x60' \
        b'\x40\x33\x0e\x1d\x61\x64\x64\x5f\x76\x6f\x74' \
        b'\x65\x00\x41\x64\x64\x20\x61\x20\x76\x6f\x74\x69\x6e\x67\x20\x6f' \
        b'\x70\x74\x69\x6f\x6e\x00\x73\x5b\x6f\x70\x74\x69\x6f\x6e\x5d\x20' \
        b'\x72\x5b\x63\x6f\x6d\x6d\x61\x6e\x64\x5d\x00\x41\x2d\x0f\x1d\x62' \
        b'\x61\x6e\x00\x42\x61\x6e\x20\x70\x6c\x61\x79\x65\x72\x20\x77\x69' \
        b'\x74\x68\x20\x49\x50\x2f\x49\x50\x20\x72\x61\x6e\x67\x65\x2f\x63' \
        b'\x6c\x69\x65\x6e\x74\x20\x69\x64\x20\x66\x6f\x72\x20\x78\x20\x6d' \
        b'\x69\x6e\x75\x74\x65\x73\x20\x66\x6f\x72\x20\x61\x6e\x79\x20\x72' \
        b'\x65\x61\x73\x6f\x6e\x00\x73\x5b\x69\x64\x7c\x69\x70\x7c\x72\x61' \
        b'\x6e\x67\x65\x5d\x20\x3f\x69\x5b\x6d\x69\x6e\x75\x74\x65\x73\x5d' \
        b'\x20\x72\x5b\x72\x65\x61\x73\x6f\x6e\x5d\x00\x40\x14\x10\x1d\x62' \
        b'\x61\x6e\x73\x00\x53\x68\x6f\x77\x20\x62\x61\x6e\x6c\x69\x73\x74' \
        b'\x00\x00\x40\x2a\x11\x1d\x62\x61\x6e\x73\x5f\x73\x61\x76\x65\x00' \
        b'\x53\x61\x76\x65\x20\x62\x61\x6e\x6c\x69\x73\x74\x20\x69\x6e\x20' \
        b'\x61\x20\x66\x69\x6c\x65\x00\x73\x5b\x66\x69\x6c\x65\x5d\x00\x40' \
        b'\x32\x12\x1d\x62\x69\x6e\x64\x61\x64\x64\x72\x00\x41\x64\x64\x72' \
        b'\x65\x73\x73\x20\x74\x6f\x20\x62\x69\x6e\x64\x20\x74\x68\x65\x20' \
        b'\x63\x6c\x69\x65\x6e\x74\x2f\x73\x65\x72\x76\x65\x72\x20\x74\x6f' \
        b'\x00\x3f\x72\x00\x40\x25\x13\x1d\x62\x72\x6f\x61\x64\x63\x61\x73' \
        b'\x74\x00\x42\x72\x6f\x61\x64\x63\x61\x73\x74\x20\x6d\x65\x73\x73' \
        b'\x61\x67\x65\x00\x72\x5b\x74\x65\x78\x74\x5d\x00\x40\x1f\x14\x1d' \
        b'\x63\x68\x61\x6e\x67\x65\x5f\x6d\x61\x70\x00\x43\x68\x61\x6e\x67' \
        b'\x65\x20\x6d\x61\x70\x00\x3f\x72\x5b\x6d\x61\x70\x5d\x00\x40\x28' \
        b'\x15\x1d\x63\x6c\x65\x61\x72\x5f\x76\x6f\x74\x65\x73\x00\x43\x6c' \
        b'\x65\x61\x72\x73\x20\x74\x68\x65\x20\x76\x6f\x74\x69\x6e\x67\x20' \
        b'\x6f\x70\x74\x69\x6f\x6e\x73\x00\x00\x41\x0a\x16\x1d\x63\x6f\x6e' \
        b'\x73\x6f\x6c\x65\x5f\x6f\x75\x74\x70\x75\x74\x5f\x6c\x65\x76\x65' \
        b'\x6c\x00\x41\x64\x6a\x75\x73\x74\x73\x20\x74\x68\x65\x20\x61\x6d' \
        b'\x6f\x75\x6e\x74\x20\x6f\x66\x20\x69\x6e\x66\x6f\x72\x6d\x61\x74' \
        b'\x69\x6f\x6e\x20\x69\x6e\x20\x74\x68\x65\x20\x63\x6f\x6e\x73\x6f' \
        b'\x6c\x65\x00\x3f\x69\x00\x40\x1d\x17\x1d\x64\x62\x67\x5f\x68\x69' \
        b'\x74\x63\x68\x00\x48\x69\x74\x63\x68\x20\x77\x61\x72\x6e\x69\x6e' \
        b'\x67\x73\x00\x3f\x69\x00\x40\x21\x18\x1d\x64\x62\x67\x5f\x6c\x6f' \
        b'\x67\x6e\x65\x74\x77\x6f\x72\x6b\x00\x4c\x6f\x67\x20\x74\x68\x65' \
        b'\x20\x6e\x65\x74\x77\x6f\x72\x6b\x00\x00\x40\x21\x19\x1d\x64\x62' \
        b'\x67\x5f\x70\x72\x65\x66\x00\x50\x65\x72\x66\x6f\x72\x6d\x61\x6e' \
        b'\x63\x65\x20\x6f\x75\x74\x70\x75\x74\x73\x00\x3f\x69\x00\x40\x1e' \
        b'\x1a\x1d\x64\x62\x67\x5f\x73\x74\x72\x65\x73\x73\x00\x53\x74\x72' \
        b'\x65\x73\x73\x20\x73\x79\x73\x74\x65\x6d\x73\x00\x3f\x69\x00\x40' \
        b'\x26\x1b\x1d\x64\x62\x67\x5f\x73\x74\x72\x65\x73\x73\x5f\x6e\x65' \
        b'\x74\x77\x6f\x72\x6b\x00\x53\x74\x72\x65\x73\x73\x20\x6e\x65\x74' \
        b'\x77\x6f\x72\x6b\x00\x3f\x69\x00\x40\x15\x1c\x1d\x64\x65\x62\x75' \
        b'\x67\x00\x44\x65\x62\x75\x67\x20\x6d\x6f\x64\x65\x00\x3f\x69\x00' \
        b'\x40\x1c\x1d\x1d\x65\x63\x68\x6f\x00\x45\x63\x68\x6f\x20\x74\x68' \
        b'\x65\x20\x74\x65\x78\x74\x00\x72\x5b\x74\x65\x78\x74\x5d\x00\x40' \
        b'\x11\x1e\x3b\x69\x64\x63\x5f\x66\x6c\x79\x6c\x61\x6e\x64\x5f\x63' \
        b'\x6f\x64\x00\x40\x0e\x1f\x3b\x5a\x61\x6e\x74\x44\x72\x61\x67\x6f' \
        b'\x72\x65\x73\x00\x40\x0d\x20\x3b\x43\x68\x69\x6c\x6c\x42\x6c\x6f' \
        b'\x63\x6b\x36\x00\x40\x0d\x21\x3b\x43\x68\x69\x6c\x6c\x42\x6c\x6f' \
        b'\x63\x6b\x5f\x00\x40\x12\x22\x3b\x30\x37\x2f\x46\x6c\x69\x70\x70' \
        b'\x69\x6e\x44\x69\x70\x70\x69\x6e\x00\x40\x0e\x23\x3b\x30\x37\x2f' \
        b'\x4d\x75\x6c\x74\x69\x63\x72\x61\x70\x00\x40\x13\x24\x3b\x30\x37' \
        b'\x2f\x61\x75\x74\x6f\x6d\x61\x70\x5f\x6a\x75\x6e\x67\x6c\x65\x00' \
        b'\x40\x11\x25\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f' \
        b'\x64\x6d\x33\x00\x40\x11\x26\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63' \
        b'\x69\x61\x6c\x2f\x64\x6d\x38\x00\x40\x11\x27\x3b\x30\x37\x2f\x6f' \
        b'\x66\x66\x69\x63\x69\x61\x6c\x2f\x64\x6d\x37\x00\x40\x11\x28\x3b' \
        b'\x30\x37\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f\x64\x6d\x31\x00' \
        b'\x40\x12\x29\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f' \
        b'\x63\x74\x66\x33\x00\x40\x12\x2a\x3b\x30\x37\x2f\x6f\x66\x66\x69' \
        b'\x63\x69\x61\x6c\x2f\x6c\x6d\x73\x31\x00\x40\x12\x2b\x3b\x30\x37' \
        b'\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f\x63\x74\x66\x38\x00\x40' \
        b'\x12\x2c\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f\x63' \
        b'\x74\x66\x36\x00\x40\x12\x2d\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63' \
        b'\x69\x61\x6c\x2f\x63\x74\x66\x35\x00\x40\x12\x2e\x3b\x30\x37\x2f' \
        b'\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f\x63\x74\x66\x32\x00\x40\x11' \
        b'\x2f\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f\x64\x6d' \
        b'\x39\x00\x40\x11\x30\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63\x69\x61' \
        b'\x6c\x2f\x64\x6d\x36\x00\x40\x12\x31\x3b\x30\x37\x2f\x6f\x66\x66' \
        b'\x69\x63\x69\x61\x6c\x2f\x63\x74\x66\x34\x00\x40\x12\x32\x3b\x30' \
        b'\x37\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f\x63\x74\x66\x31\x00' \
        b'\x40\x12\x33\x3b\x30\x37\x2f\x6f\x66\x66\x69\x63\x69\x61\x6c\x2f' \
        b'\x63\x74\x66\x37\x00\x40\x11\x34\x3b\x30\x37\x2f\x6f\x66\x66\x69' \
        b'\x63\x69\x61\x6c\x2f\x64\x6d\x32\x00\x40\x11\x35\x3b\x30\x37\x2f' \
        b'\x66\x6c\x79\x69\x6e\x67\x32\x7a\x69\x6c\x6c\x79\x00\x40\x0a\x36' \
        b'\x3b\x30\x37\x2f\x74\x65\x73\x74\x34\x00\x40\x10\x37\x3b\x30\x37' \
        b'\x2f\x43\x68\x69\x6c\x6c\x47\x72\x6f\x75\x6e\x64\x00\x40\x0c\x38' \
        b'\x3b\x30\x37\x2f\x43\x68\x69\x6c\x6c\x6c\x6a\x00\x40\x0e\x39\x3b' \
        b'\x30\x37\x2f\x43\x72\x61\x63\x6b\x43\x61\x6d\x70\x00\x40\x21\x3a' \
        b'\x3b\x42\x6c\x6d\x61\x70\x43\x68\x69\x6c\x6c\x2f\x42\x6c\x6d\x61' \
        b'\x70\x43\x68\x69\x6c\x6c\x5f\x6f\x6c\x64\x73\x63\x68\x6f\x6f\x6c' \
        b'\x00\x40\x21\x3b\x3b\x42\x6c\x6d\x61\x70\x43\x68\x69\x6c\x6c\x2f' \
        b'\x42\x6c\x6d\x61\x70\x43\x68\x69\x6c\x6c\x5f\x66\x6f\x72\x6b\x5f' \
        b'\x61\x30\x30\x31\x00\x40\x1d\x3c\x3b\x42\x6c\x6d\x61\x70\x43\x68' \
        b'\x69\x6c\x6c\x2f\x42\x6c\x6d\x61\x70\x43\x68\x69\x6c\x6c\x5f\x74' \
        b'\x65\x73\x74\x79\x00\x40\x1b\x3d\x3b\x42\x6c\x6d\x61\x70\x43\x68' \
        b'\x69\x6c\x6c\x2f\x42\x6c\x6d\x61\x70\x43\x68\x69\x6c\x6c\x5f\x76' \
        b'\x32\x32\x00')

    assert packet.version == '0.7'

    assert packet.header.token == b'\x4d\xcb\x93\x60'
    assert packet.header.num_chunks == 48
    assert len(packet.messages) == 48
    assert packet.header.ack == 5

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is False

    # rcon commands

    cmd = packet.messages[0]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'add_vote'
    assert cmd.help == 'Add a voting option'
    assert cmd.params == 's[option] r[command]'

    cmd = packet.messages[1]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'ban'
    assert cmd.help == \
        'Ban player with IP/IP range/client id for x minutes for any reason'
    assert cmd.params == 's[id|ip|range] ?i[minutes] r[reason]'

    cmd = packet.messages[2]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'bans'
    assert cmd.help == 'Show banlist'
    assert cmd.params == ''

    cmd = packet.messages[3]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'bans_save'
    assert cmd.help == 'Save banlist in a file'
    assert cmd.params == 's[file]'

    cmd = packet.messages[4]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'bindaddr'
    assert cmd.help == 'Address to bind the client/server to'
    assert cmd.params == '?r'

    cmd = packet.messages[5]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'broadcast'
    assert cmd.help == 'Broadcast message'
    assert cmd.params == 'r[text]'

    cmd = packet.messages[6]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'change_map'
    assert cmd.help == 'Change map'
    assert cmd.params == '?r[map]'

    cmd = packet.messages[7]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'clear_votes'
    assert cmd.help == 'Clears the voting options'
    assert cmd.params == ''

    cmd = packet.messages[8]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'console_output_level'
    assert cmd.help == 'Adjusts the amount of information in the console'
    assert cmd.params == '?i'

    cmd = packet.messages[9]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'dbg_hitch'
    assert cmd.help == 'Hitch warnings'
    assert cmd.params == '?i'

    cmd = packet.messages[10]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'dbg_lognetwork'
    assert cmd.help == 'Log the network'
    assert cmd.params == ''

    cmd = packet.messages[11]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'dbg_pref'
    assert cmd.help == 'Performance outputs'
    assert cmd.params == '?i'

    cmd = packet.messages[12]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'dbg_stress'
    assert cmd.help == 'Stress systems'
    assert cmd.params == '?i'

    cmd = packet.messages[13]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'dbg_stress_network'
    assert cmd.help == 'Stress network'
    assert cmd.params == '?i'

    cmd = packet.messages[14]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'debug'
    assert cmd.help == 'Debug mode'
    assert cmd.params == '?i'

    cmd = packet.messages[15]
    assert cmd.message_name == 'rcon_cmd_add'
    assert cmd.name == 'echo'
    assert cmd.help == 'Echo the text'
    assert cmd.params == 'r[text]'

    # maps

    map = packet.messages[16]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'idc_flyland_cod'

    map = packet.messages[17]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'ZantDragores'

    map = packet.messages[18]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'ChillBlock6'

    map = packet.messages[19]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'ChillBlock_'

    map = packet.messages[20]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/FlippinDippin'

    map = packet.messages[21]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/Multicrap'

    map = packet.messages[22]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/automap_jungle'

    map = packet.messages[23]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/dm3'

    map = packet.messages[24]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/dm8'

    map = packet.messages[25]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/dm7'

    map = packet.messages[26]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/dm1'

    map = packet.messages[27]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf3'

    map = packet.messages[28]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/lms1'

    map = packet.messages[29]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf8'

    map = packet.messages[30]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf6'

    map = packet.messages[31]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf5'

    map = packet.messages[32]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf2'

    map = packet.messages[33]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/dm9'

    map = packet.messages[34]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/dm6'

    map = packet.messages[35]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf4'

    map = packet.messages[36]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf1'

    map = packet.messages[37]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/ctf7'

    map = packet.messages[38]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/official/dm2'

    map = packet.messages[39]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/flying2zilly'

    map = packet.messages[40]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/test4'

    map = packet.messages[41]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/ChillGround'

    map = packet.messages[42]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/Chilllj'

    map = packet.messages[43]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == '07/CrackCamp'

    map = packet.messages[44]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'BlmapChill/BlmapChill_oldschool'

    map = packet.messages[45]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'BlmapChill/BlmapChill_fork_a001'

    map = packet.messages[46]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'BlmapChill/BlmapChill_testy'

    map = packet.messages[47]
    assert map.message_name == 'maplist_entry_add'
    assert map.name == 'BlmapChill/BlmapChill_v22'

    try:
        assert packet.messages[48].message_name == 'out of range'
    except IndexError:
        pass
