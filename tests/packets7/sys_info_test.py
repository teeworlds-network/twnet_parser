from twnet_parser.packet import parse7, TwPacket7
from twnet_parser.messages7.system.info import MsgInfo

from typing import cast

# Not sure if this is even needed
# def test_parse_info7_handcrafted_no_pass_and_version() -> None:
#     """
#     libtw2 spec states that the password
#     and client version fields are optional
# 
#     so we should not crash if those are not set
#     """
#     data = b'\x00\x00\x01H\x1f\x93\xd7\x00\x19\x030.7 802f1be60a05665f'
#     packet: TwPacket = parse7(data)
#     assert len(packet.messages) == 1
#     msg: MsgInfo = cast(MsgInfo, packet.messages[0])
#     assert msg.message_name == 'info'
#     assert msg.version == "0.7 802f1be60a05665"
#     assert msg.password == ''

def test_pack_info7() -> None:
    packet: TwPacket7 = TwPacket7()
    packet.header.token = b'\x48\x1f\x93\xd7'
    info = MsgInfo(
            version = "0.7 802f1be60a05665f",
            client_version = 1797
    )
    packet.messages.append(info)

    data = packet.pack()
    assert data == b'\x00\x00\x01H\x1f\x93\xd7\x00\x19\x030.7 802f1be60a05665f\x00\x00\x85\x1c'

    packet: TwPacket7 = parse7(data)
    assert packet.version == '0.7'

    assert packet.header.token == b'\x48\x1f\x93\xd7'
    assert packet.header.num_chunks == 1
    assert packet.header.ack == 0

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False

    assert len(packet.messages) == 1

    msg: MsgInfo = cast(MsgInfo, packet.messages[0])

    assert msg.message_name == 'info'

    assert msg.version == "0.7 802f1be60a05665f"
    assert msg.password == ''
    assert msg.client_version == 1797

    assert msg.header.flags.resend is False
    assert msg.header.flags.vital is False # TODO: this should be True by default
    assert msg.header.size == 25
    assert msg.header.seq == -1 # TODO: that should not be negative by default

