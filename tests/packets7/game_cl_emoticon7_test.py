from twnet_parser.packet import parse7, TwPacket7
from twnet_parser.messages7.game.cl_emoticon import MsgClEmoticon
from twnet_parser.messages7.system.input import MsgInput

def test_parse_7_real_cl_emote_and_sys_input() -> None:
    """
    This is the raw compressed data copied from a tcpdump
    of a vanilla 0.7.5 client talking to a vanilla server
    """

    data: bytes = \
        b'\x10\x0a' \
        b'\x02\x35\x9f\x75\x99\x4a\xc2\xee\xc3\xdd\x9a\xd2\x51\xa8\x76\x99' \
        b'\xda\x1a\xff\x19\x29\x6e\x00' \

    packet: TwPacket7 = parse7(data)

    assert packet.version == '0.7'

    assert packet.header.token == b'\x35\x9f\x75\x99'
    assert packet.header.num_chunks == 2
    assert packet.header.ack == 10

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is True
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is False

    assert len(packet.messages) == 2

    emote: MsgClEmoticon = packet.messages[0]
    assert emote.emoticon == 7

    input: MsgInput = packet.messages[1]
    assert input.ack_snapshot == 1078
    assert input.intended_tick == 1081
    assert input.input_size == 40

