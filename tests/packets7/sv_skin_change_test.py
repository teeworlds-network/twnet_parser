from twnet_parser.packet import parse7

def test_parse_7_real_sv_skin_change():
    packet = parse7(b'\x00\x0e\x02\x44\xac\x9b\x27' \
        b'\x41\x0d\x0d\x82\x01\x00\x67\x72\x65\x65\x6e' \
        b'\x73\x77\x61\x72\x64\x00\x64\x75\x6f\x64\x6f\x6e\x6e\x79\x00\x00' \
        b'\x73\x74\x61\x6e\x64\x61\x72\x64\x00\x73\x74\x61\x6e\x64\x61\x72' \
        b'\x64\x00\x73\x74\x61\x6e\x64\x61\x72\x64\x00\x01\x01\x00\x00\x00' \
        b'\x00\x80\xfc\xaf\x05\xeb\x83\xd0\x0a\x80\xfe\x07\x80\xfe\x07\x80' \
        b'\xfe\x07\x80\xfe\x07\x00\x04\x0f\xbc\x3e\x0e')

    assert packet.version == '0.7'

    assert packet.header.token == b'\x44\xac\x9b\x27'
    assert packet.header.num_chunks == 2
    assert packet.header.ack == 14

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is False

    assert len(packet.messages) == 2

    msg = packet.messages[0]
    assert msg.system_message is False
    assert msg.message_name == 'sv_skin_change'
    assert msg.client_id == 0

    assert len(msg.skin_part_names) == 6
    assert msg.skin_part_names[0] == 'greensward'
    assert msg.skin_part_names[1] == 'duodonny'
    assert msg.skin_part_names[2] == ''
    assert msg.skin_part_names[3] == 'standard'
    assert msg.skin_part_names[4] == 'standard'
    assert msg.skin_part_names[5] == 'standard'

    try:
        msg.skin_part_names[6] == 'out of range'
    except IndexError:
        pass

    assert len(msg.use_custom_colors) == 6
    assert msg.use_custom_colors[0] is True
    assert msg.use_custom_colors[1] is True
    assert msg.use_custom_colors[2] is False
    assert msg.use_custom_colors[3] is False
    assert msg.use_custom_colors[4] is False
    assert msg.use_custom_colors[5] is False

    assert len(msg.skin_part_colors) == 6
    assert msg.skin_part_colors[0] == 5635840
    assert msg.skin_part_colors[1] == -11141356
    assert msg.skin_part_colors[2] == 65408
    assert msg.skin_part_colors[3] == 65408
    assert msg.skin_part_colors[4] == 65408
    assert msg.skin_part_colors[5] == 65408

    snap = packet.messages[1]
    assert snap.message_name == 'snap_empty'
    assert snap.tick == 4028
    assert snap.delta_tick == 14
