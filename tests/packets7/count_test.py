from typing import cast

from twnet_parser.packet import parse7, TwPacket7
from twnet_parser.messages7.connless.count import MsgCount

def test_pack_handmade_count_connless7() -> None:
    packet = TwPacket7()
    packet.header.token = b'\xaa\xbb\xcc\xdd'
    packet.header.response_token = b'\xff\xaa\xff\xaa'
    packet.messages.append(MsgCount(count = 255))
    data: bytes = packet.pack()

    assert data == b'!\xaa\xbb\xcc\xdd\xff\xaa\xff\xaa\xff\xff\xff\xffsiz2\x00\xff'

    packet2 = parse7(data)

    assert packet.version == packet2.version
    assert vars(packet.messages[0]) == vars(packet2.messages[0])

    assert len(packet2.messages) == 1
    assert packet2.messages[0].message_name == 'connless.count'
    assert packet2.messages[0].count == 255

def test_unpack_real_count_connless7() -> None:
    data = b'\x21\x8f\x4b\xc5\xb1\x5c\x8d\x6c\xfd' \
            b'\xff\xff\xff\xff\x73\x69\x7a\x32\x00\x2b'

    packet = parse7(data)

    assert packet.version == '0.7'

    assert packet.header.token == b'\x8f\x4b\xc5\xb1'
    assert packet.header.response_token == b'\x5c\x8d\x6c\xfd'

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is True

    assert len(packet.messages) == 1

    msg: MsgCount = cast(MsgCount, packet.messages[0])

    assert msg.message_name == 'connless.count'
    assert msg.count == 43

    repack: bytes = packet.pack()

    assert repack == data

