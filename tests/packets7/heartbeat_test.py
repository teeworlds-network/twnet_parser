from twnet_parser.packet import parse7

def test_heartbeat_connless7():
    data = b'\x21\x5c\x8d\x6c\xfd\x8f\x4b\xc5\xb1' \
        b'\xff\xff\xff\xff\x62\x65\x61\x32\x20\x6f'

    packet = parse7(data)

    assert packet.version == '0.7'

    assert packet.header.token == b'\x5c\x8d\x6c\xfd'
    assert packet.header.response_token == b'\x8f\x4b\xc5\xb1'

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is True

    assert len(packet.messages) == 1

    msg: MsgList = packet.messages[0]

    assert msg.message_name == 'connless.heartbeat'
    assert msg.alt_port == 8303

    repack: bytes = packet.pack()

    assert repack == data
