from twnet_parser.packet import parse7

def test_parse_7_real_ddnet_07_bridge_packet():
    data = b'\x00\x01\x04\xff\xaa\xbb\xee' \
        b'\x40\x12\x01\x01\x12\x81\x0e\x1f\xa1\xdb\x33\x78\xb4\xfb\x16' \
        b'\x4e\xd6\x50\x59\x26\x00\x40\x13\x02\x01\xf6\x21\xa5\xa1\xf5\x85' \
        b'\x37\x75\x8e\x73\x41\xbe\xee\x79\xf2\xb2\x05\x3f\x41\x02\x03\x01' \
        b'\xf9\x11\x7b\x3c\x80\x39\x34\x16\x9f\xc0\xae\xf2\xbc\xb7\x5c\x03' \
        b'\x73\x75\x72\x76\x69\x76\x61\x6c\x00\xd8\x00\x26\xb9\x48\x92\xbe' \
        b'\x2e\xed\xef\xa7\x41\x13\xe2\x70\x7a\x5b\x87\xd6\xaa\x71\x27\xee' \
        b'\xc6\x49\xcc\xa7\xe8\xe8\x8b\xa1\x0a\x8a\xf7\xa0\x6b\x9c\x9d\x17' \
        b'\x00\x40\x34\x04\x05\x73\x75\x72\x76\x69\x76\x61\x6c\x00\x8a\xf7' \
        b'\xa0\x6b\x9c\x9d\x17\x0f\x80\x0e\xd8\x00\x26\xb9\x48\x92\xbe\x2e' \
        b'\xed\xef\xa7\x41\x13\xe2\x70\x7a\x5b\x87\xd6\xaa\x71\x27\xee\xc6' \
        b'\x49\xcc\xa7\xe8\xe8\x8b\xa1\x0a'
    packet = parse7(data)

    # Teeworlds 0.7 Protocol chunk: sys.12810e1f-a1db-3378-b4fb-164ed6505926
    # Teeworlds 0.7 Protocol chunk: sys.f621a5a1-f585-3775-8e73-41beee79f2b2
    # Teeworlds 0.7 Protocol chunk: sys.f9117b3c-8039-3416-9fc0-aef2bcb75c03
    # Teeworlds 0.7 Protocol chunk: sys.map_change

    assert packet.version == '0.7'

    assert packet.header.token == b'\xff\xaa\xbb\xee'
    assert packet.header.num_chunks == 4
    assert packet.header.ack == 1

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False
    assert packet.header.flags.resend is False
    assert packet.header.flags.connless is False

    assert len(packet.messages) == 4

    msg = packet.messages[0]
    assert msg.message_name == 'ddnet_uuid'
    msg = packet.messages[1]
    assert msg.message_name == 'ddnet_uuid'
    msg = packet.messages[2]
    assert msg.message_name == 'ddnet_uuid'

    msg = packet.messages[3]
    assert msg.system_message is True
    assert msg.message_name == 'map_change'

