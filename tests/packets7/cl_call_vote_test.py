from twnet_parser.packet import parse7, TwPacket7
from twnet_parser.messages7.game.cl_call_vote import MsgClCallVote

from typing import cast

def test_parse_7_real_call_vote() -> None:
    packet: TwPacket7 = parse7(b'\x02\x7e\x01\x48\x1f\x93\xd7' \
        b'\x40\x10\x0a' \
        b'\x80\x01' \
        b'\x6f\x70\x74\x69\x6f\x6e\x00' \
        b'\x74\x65\x73\x74\x00' \
        b'\x00' \
        b'\x00')

    assert packet.version == '0.7'

    assert packet.header.token == b'\x48\x1f\x93\xd7'
    assert packet.header.num_chunks == 1
    assert packet.header.ack == 638

    assert packet.header.flags.control is False
    assert packet.header.flags.compression is False

    assert len(packet.messages) == 1

    msg: MsgClCallVote = cast(MsgClCallVote, packet.messages[0])

    assert msg.message_name == 'cl_call_vote'

    assert msg.type == 'option'
    assert msg.value == 'test'
    assert msg.reason == ''
    assert msg.force is False

    assert msg.header.flags.resend is False
    assert msg.header.flags.vital is True
    assert msg.header.size == 16
    assert msg.header.seq == 10

def test_serialize_7_real_call_vote_all_fields_set() -> None:
    packet: TwPacket7 = TwPacket7()
    packet.version == '0.7'

    packet.header.token = b'\x48\x1f\x93\xd7'
    packet.header.num_chunks = 1
    packet.header.ack = 638

    packet.header.flags.control = False
    packet.header.flags.compression = False

    msg: MsgClCallVote = MsgClCallVote()

    msg.header.flags.resend = False
    msg.header.flags.vital = True
    msg.header.size = 16
    msg.header.seq = 10

    msg.type = 'option'
    msg.value = 'test'
    msg.reason = ''
    msg.force = False

    packet.messages.append(msg)

    data: bytes = b'\x02\x7e\x01\x48\x1f\x93\xd7' \
        b'\x40\x10\x0a' \
        b'\x80\x01' \
        b'\x6f\x70\x74\x69\x6f\x6e\x00' \
        b'\x74\x65\x73\x74\x00' \
        b'\x00' \
        b'\x00'

    assert packet.pack() == data

def test_serialize_7_real_call_vote_allow_custom_size() -> None:
    packet: TwPacket7 = TwPacket7()
    packet.version == '0.7'

    packet.header.token = b'\x48\x1f\x93\xd7'
    packet.header.num_chunks = 1
    packet.header.ack = 638

    packet.header.flags.control = False
    packet.header.flags.compression = False

    msg: MsgClCallVote = MsgClCallVote()

    msg.header.flags.resend = False
    msg.header.flags.vital = True

    # this is technically wrong
    # but the library should allow sending
    # wrong values
    msg.header.size = 6
    msg.header.seq = 10

    msg.type = 'option'
    msg.value = 'test'
    msg.reason = ''
    msg.force = False

    packet.messages.append(msg)

    data: bytes = b'\x02\x7e\x01\x48\x1f\x93\xd7' \
        b'\x40\x06\x0a' \
        b'\x80\x01' \
        b'\x6f\x70\x74\x69\x6f\x6e\x00' \
        b'\x74\x65\x73\x74\x00' \
        b'\x00' \
        b'\x00'

    assert packet.pack() == data

def test_serialize_7_real_call_vote_should_allow_custom_num_chunks() -> None:
    packet: TwPacket7 = TwPacket7()
    packet.version == '0.7'

    packet.header.token = b'\x48\x1f\x93\xd7'

    # this is technically wrong
    # but the library should allow sending
    # wrong values
    packet.header.num_chunks = 6
    packet.header.ack = 638

    packet.header.flags.control = False
    packet.header.flags.compression = False

    msg: MsgClCallVote = MsgClCallVote()

    msg.header.flags.resend = False
    msg.header.flags.vital = True
    msg.header.size = 16
    msg.header.seq = 10

    msg.type = 'option'
    msg.value = 'test'
    msg.reason = ''
    msg.force = False

    packet.messages.append(msg)

    data: bytes = b'\x02\x7e\x06\x48\x1f\x93\xd7' \
        b'\x40\x10\x0a' \
        b'\x80\x01' \
        b'\x6f\x70\x74\x69\x6f\x6e\x00' \
        b'\x74\x65\x73\x74\x00' \
        b'\x00' \
        b'\x00'

    assert packet.pack() == data

def test_serialize_7_real_call_vote_compute_size() -> None:
    packet: TwPacket7 = TwPacket7()
    packet.version == '0.7'

    packet.header.token = b'\x48\x1f\x93\xd7'
    packet.header.num_chunks = 1
    packet.header.ack = 638

    packet.header.flags.control = False
    packet.header.flags.compression = False

    msg: MsgClCallVote = MsgClCallVote()

    msg.header.flags.resend = False
    msg.header.flags.vital = True

    # This should be computed and overwritten by
    # TwPacket7.pack()
    # msg.header.size = 16
    msg.header.seq = 10

    msg.type = 'option'
    msg.value = 'test'
    msg.reason = ''
    msg.force = False

    packet.messages.append(msg)

    data: bytes = b'\x02\x7e\x01\x48\x1f\x93\xd7' \
        b'\x40\x10\x0a' \
        b'\x80\x01' \
        b'\x6f\x70\x74\x69\x6f\x6e\x00' \
        b'\x74\x65\x73\x74\x00' \
        b'\x00' \
        b'\x00'

    assert packet.pack() == data

def test_serialize_7_real_call_vote_compute_defaults() -> None:
    packet: TwPacket7 = TwPacket7()
    packet.version == '0.7'

    packet.header.token = b'\x48\x1f\x93\xd7'
    packet.header.num_chunks = None # <= compute this
    packet.header.ack = 638

    packet.header.flags.control = False
    packet.header.flags.compression = False

    msg: MsgClCallVote = MsgClCallVote()

    msg.header.flags.resend = False
    msg.header.flags.vital = True

    msg.header.size = None # <= compute this
    msg.header.seq = 10

    msg.type = 'option'
    msg.value = 'test'
    msg.reason = ''
    msg.force = False

    packet.messages.append(msg)

    data: bytes = b'\x02\x7e\x01\x48\x1f\x93\xd7' \
        b'\x40\x10\x0a' \
        b'\x80\x01' \
        b'\x6f\x70\x74\x69\x6f\x6e\x00' \
        b'\x74\x65\x73\x74\x00' \
        b'\x00' \
        b'\x00'

    assert packet.pack() == data
