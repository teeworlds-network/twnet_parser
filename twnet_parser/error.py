class ParserError(Exception):
    """
    This error is thrown when there is an error
    unpacking teeworlds traffic
    """

class PackerError(Exception):
    """
    This error is thrown when there is an error
    packing teeworlds traffic
    """
