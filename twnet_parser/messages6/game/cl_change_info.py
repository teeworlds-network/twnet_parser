# generated by scripts/generate_messages.py

from twnet_parser.pretty_print import PrettyPrint
from twnet_parser.packer import Unpacker
from twnet_parser.chunk_header import ChunkHeader
from twnet_parser.packer import SANITIZE_CC, pack_int, pack_str
from typing import Literal, Optional

class MsgClChangeInfo(PrettyPrint):
    def __init__(
            self,
            chunk_header: Optional[ChunkHeader] = None,
            name: str = 'default',
            clan: str = 'default',
            country: int = 0,
            skin: str = 'default',
            use_custom_color: bool = False,
            color_body: int = 0,
            color_feet: int = 0
    ) -> None:
        self.message_type: Literal['system', 'game'] = 'game'
        self.message_name: str = 'cl_change_info'
        self.system_message: bool = False
        self.message_id: int = 21
        if not chunk_header:
            chunk_header = ChunkHeader(version = '0.6')
        self.header: ChunkHeader = chunk_header

        self.name: str = name
        self.clan: str = clan
        self.country: int = country
        self.skin: str = skin
        self.use_custom_color: bool = use_custom_color
        self.color_body: int = color_body
        self.color_feet: int = color_feet

    def __iter__(self):
        yield 'message_type', self.message_type
        yield 'message_name', self.message_name
        yield 'system_message', self.system_message
        yield 'message_id', self.message_id
        yield 'header', dict(self.header)

        yield 'name', self.name
        yield 'clan', self.clan
        yield 'country', self.country
        yield 'skin', self.skin
        yield 'use_custom_color', self.use_custom_color
        yield 'color_body', self.color_body
        yield 'color_feet', self.color_feet

    # first byte of data
    # has to be the first byte of the message payload
    # NOT the chunk header and NOT the message id
    def unpack(self, data: bytes) -> bool:
        unpacker = Unpacker(data)
        self.name = unpacker.get_str(SANITIZE_CC)
        self.clan = unpacker.get_str(SANITIZE_CC)
        self.country = unpacker.get_int()
        self.skin = unpacker.get_str(SANITIZE_CC)
        self.use_custom_color = unpacker.get_int() == 1
        self.color_body = unpacker.get_int()
        self.color_feet = unpacker.get_int()
        return True

    def pack(self) -> bytes:
        return pack_str(self.name) + \
            pack_str(self.clan) + \
            pack_int(self.country) + \
            pack_str(self.skin) + \
            pack_int(self.use_custom_color) + \
            pack_int(self.color_body) + \
            pack_int(self.color_feet)