# generated by scripts/generate_messages.py
from typing import Optional

import twnet_parser.msg6
from twnet_parser.net_message import NetMessage
from twnet_parser.ddnet_uuid import MsgDDNetUuid

import twnet_parser.messages6.game.sv_motd as \
       game6_sv_motd
import twnet_parser.messages6.game.sv_broadcast as \
       game6_sv_broadcast
import twnet_parser.messages6.game.sv_chat as \
       game6_sv_chat
import twnet_parser.messages6.game.sv_kill_msg as \
       game6_sv_kill_msg
import twnet_parser.messages6.game.sv_sound_global as \
       game6_sv_sound_global
import twnet_parser.messages6.game.sv_tune_params as \
       game6_sv_tune_params
import twnet_parser.messages6.game.sv_extra_projectile as \
       game6_sv_extra_projectile
import twnet_parser.messages6.game.sv_ready_to_enter as \
       game6_sv_ready_to_enter
import twnet_parser.messages6.game.sv_weapon_pickup as \
       game6_sv_weapon_pickup
import twnet_parser.messages6.game.sv_emoticon as \
       game6_sv_emoticon
import twnet_parser.messages6.game.sv_vote_clear_options as \
       game6_sv_vote_clear_options
import twnet_parser.messages6.game.sv_vote_option_list_add as \
       game6_sv_vote_option_list_add
import twnet_parser.messages6.game.sv_vote_option_add as \
       game6_sv_vote_option_add
import twnet_parser.messages6.game.sv_vote_option_remove as \
       game6_sv_vote_option_remove
import twnet_parser.messages6.game.sv_vote_set as \
       game6_sv_vote_set
import twnet_parser.messages6.game.sv_vote_status as \
       game6_sv_vote_status
import twnet_parser.messages6.game.cl_say as \
       game6_cl_say
import twnet_parser.messages6.game.cl_set_team as \
       game6_cl_set_team
import twnet_parser.messages6.game.cl_set_spectator_mode as \
       game6_cl_set_spectator_mode
import twnet_parser.messages6.game.cl_start_info as \
       game6_cl_start_info
import twnet_parser.messages6.game.cl_change_info as \
       game6_cl_change_info
import twnet_parser.messages6.game.cl_kill as \
       game6_cl_kill
import twnet_parser.messages6.game.cl_emoticon as \
       game6_cl_emoticon
import twnet_parser.messages6.game.cl_vote as \
       game6_cl_vote
import twnet_parser.messages6.game.cl_call_vote as \
       game6_cl_call_vote

def match_game6(msg_id: int, data: bytes) -> NetMessage:
    msg: Optional[NetMessage] = None

    if msg_id == twnet_parser.msg6.SV_MOTD:
        msg = game6_sv_motd.MsgSvMotd()
    elif msg_id == twnet_parser.msg6.SV_BROADCAST:
        msg = game6_sv_broadcast.MsgSvBroadcast()
    elif msg_id == twnet_parser.msg6.SV_CHAT:
        msg = game6_sv_chat.MsgSvChat()
    elif msg_id == twnet_parser.msg6.SV_KILL_MSG:
        msg = game6_sv_kill_msg.MsgSvKillMsg()
    elif msg_id == twnet_parser.msg6.SV_SOUND_GLOBAL:
        msg = game6_sv_sound_global.MsgSvSoundGlobal()
    elif msg_id == twnet_parser.msg6.SV_TUNE_PARAMS:
        msg = game6_sv_tune_params.MsgSvTuneParams()
    elif msg_id == twnet_parser.msg6.SV_EXTRA_PROJECTILE:
        msg = game6_sv_extra_projectile.MsgSvExtraProjectile()
    elif msg_id == twnet_parser.msg6.SV_READY_TO_ENTER:
        msg = game6_sv_ready_to_enter.MsgSvReadyToEnter()
    elif msg_id == twnet_parser.msg6.SV_WEAPON_PICKUP:
        msg = game6_sv_weapon_pickup.MsgSvWeaponPickup()
    elif msg_id == twnet_parser.msg6.SV_EMOTICON:
        msg = game6_sv_emoticon.MsgSvEmoticon()
    elif msg_id == twnet_parser.msg6.SV_VOTE_CLEAR_OPTIONS:
        msg = game6_sv_vote_clear_options.MsgSvVoteClearOptions()
    elif msg_id == twnet_parser.msg6.SV_VOTE_OPTION_LIST_ADD:
        msg = game6_sv_vote_option_list_add.MsgSvVoteOptionListAdd()
    elif msg_id == twnet_parser.msg6.SV_VOTE_OPTION_ADD:
        msg = game6_sv_vote_option_add.MsgSvVoteOptionAdd()
    elif msg_id == twnet_parser.msg6.SV_VOTE_OPTION_REMOVE:
        msg = game6_sv_vote_option_remove.MsgSvVoteOptionRemove()
    elif msg_id == twnet_parser.msg6.SV_VOTE_SET:
        msg = game6_sv_vote_set.MsgSvVoteSet()
    elif msg_id == twnet_parser.msg6.SV_VOTE_STATUS:
        msg = game6_sv_vote_status.MsgSvVoteStatus()
    elif msg_id == twnet_parser.msg6.CL_SAY:
        msg = game6_cl_say.MsgClSay()
    elif msg_id == twnet_parser.msg6.CL_SET_TEAM:
        msg = game6_cl_set_team.MsgClSetTeam()
    elif msg_id == twnet_parser.msg6.CL_SET_SPECTATOR_MODE:
        msg = game6_cl_set_spectator_mode.MsgClSetSpectatorMode()
    elif msg_id == twnet_parser.msg6.CL_START_INFO:
        msg = game6_cl_start_info.MsgClStartInfo()
    elif msg_id == twnet_parser.msg6.CL_CHANGE_INFO:
        msg = game6_cl_change_info.MsgClChangeInfo()
    elif msg_id == twnet_parser.msg6.CL_KILL:
        msg = game6_cl_kill.MsgClKill()
    elif msg_id == twnet_parser.msg6.CL_EMOTICON:
        msg = game6_cl_emoticon.MsgClEmoticon()
    elif msg_id == twnet_parser.msg6.CL_VOTE:
        msg = game6_cl_vote.MsgClVote()
    elif msg_id == twnet_parser.msg6.CL_CALL_VOTE:
        msg = game6_cl_call_vote.MsgClCallVote()

    # msg_id 0 is used by ddnet uuid messages
    # do not crash on those
    if msg is None:
        if msg_id != 0:
            raise ValueError(f"Error: unknown game message id={msg_id} data={data.hex(sep = ' ')}")
        msg = MsgDDNetUuid()

    msg.unpack(data)
    return msg
