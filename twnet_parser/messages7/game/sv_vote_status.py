# generated by scripts/generate_messages.py

from twnet_parser.pretty_print import PrettyPrint
from twnet_parser.packer import Unpacker
from twnet_parser.chunk_header import ChunkHeader
from twnet_parser.packer import pack_int
from typing import Literal, Optional

class MsgSvVoteStatus(PrettyPrint):
    def __init__(
            self,
            chunk_header: Optional[ChunkHeader] = None,
            yes: int = 0,
            no: int = 0,
            pass_: int = 0,
            total: int = 0
    ) -> None:
        self.message_type: Literal['system', 'game'] = 'game'
        self.message_name: str = 'sv_vote_status'
        self.system_message: bool = False
        self.message_id: int = 16
        if not chunk_header:
            chunk_header = ChunkHeader(version = '0.7')
        self.header: ChunkHeader = chunk_header

        self.yes: int = yes
        self.no: int = no
        self.pass_: int = pass_
        self.total: int = total

    def __iter__(self):
        yield 'message_type', self.message_type
        yield 'message_name', self.message_name
        yield 'system_message', self.system_message
        yield 'message_id', self.message_id
        yield 'header', dict(self.header)

        yield 'yes', self.yes
        yield 'no', self.no
        yield 'pass_', self.pass_
        yield 'total', self.total

    # first byte of data
    # has to be the first byte of the message payload
    # NOT the chunk header and NOT the message id
    def unpack(self, data: bytes) -> bool:
        unpacker = Unpacker(data)
        self.yes = unpacker.get_int()
        self.no = unpacker.get_int()
        self.pass_ = unpacker.get_int()
        self.total = unpacker.get_int()
        return True

    def pack(self) -> bytes:
        return pack_int(self.yes) + \
            pack_int(self.no) + \
            pack_int(self.pass_) + \
            pack_int(self.total)