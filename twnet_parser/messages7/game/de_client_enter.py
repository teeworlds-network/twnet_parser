# generated by scripts/generate_messages.py

from twnet_parser.pretty_print import PrettyPrint
from twnet_parser.packer import Unpacker
from twnet_parser.chunk_header import ChunkHeader
from twnet_parser.packer import SANITIZE_CC, pack_int, pack_str
from typing import Literal, Optional
import twnet_parser.enum7 as enum7

class MsgDeClientEnter(PrettyPrint):
    def __init__(
            self,
            chunk_header: Optional[ChunkHeader] = None,
            name: str = 'default',
            client_id: int = 0,
            team: int = enum7.Team.SPECTATORS.value
    ) -> None:
        self.message_type: Literal['system', 'game'] = 'game'
        self.message_name: str = 'de_client_enter'
        self.system_message: bool = False
        self.message_id: int = 22
        if not chunk_header:
            chunk_header = ChunkHeader(version = '0.7')
        self.header: ChunkHeader = chunk_header

        self.name: str = name
        self.client_id: int = client_id
        self.team: int = team

    def __iter__(self):
        yield 'message_type', self.message_type
        yield 'message_name', self.message_name
        yield 'system_message', self.system_message
        yield 'message_id', self.message_id
        yield 'header', dict(self.header)

        yield 'name', self.name
        yield 'client_id', self.client_id
        yield 'team', self.team

    # first byte of data
    # has to be the first byte of the message payload
    # NOT the chunk header and NOT the message id
    def unpack(self, data: bytes) -> bool:
        unpacker = Unpacker(data)
        self.name = unpacker.get_str(SANITIZE_CC)
        self.client_id = unpacker.get_int()
        self.team = unpacker.get_int() # enum TEAM
        return True

    def pack(self) -> bytes:
        return pack_str(self.name) + \
            pack_int(self.client_id) + \
            pack_int(self.team)