# generated by scripts/generate_messages.py

from twnet_parser.pretty_print import PrettyPrint
from twnet_parser.packer import Unpacker
from twnet_parser.chunk_header import ChunkHeader
from twnet_parser.packer import SANITIZE_CC, pack_str
from typing import Literal, Optional

class MsgClCommand(PrettyPrint):
    def __init__(
            self,
            chunk_header: Optional[ChunkHeader] = None,
            name: str = 'default',
            arguments: str = 'default'
    ) -> None:
        self.message_type: Literal['system', 'game'] = 'game'
        self.message_name: str = 'cl_command'
        self.system_message: bool = False
        self.message_id: int = 39
        if not chunk_header:
            chunk_header = ChunkHeader(version = '0.7')
        self.header: ChunkHeader = chunk_header

        self.name: str = name
        self.arguments: str = arguments

    def __iter__(self):
        yield 'message_type', self.message_type
        yield 'message_name', self.message_name
        yield 'system_message', self.system_message
        yield 'message_id', self.message_id
        yield 'header', dict(self.header)

        yield 'name', self.name
        yield 'arguments', self.arguments

    # first byte of data
    # has to be the first byte of the message payload
    # NOT the chunk header and NOT the message id
    def unpack(self, data: bytes) -> bool:
        unpacker = Unpacker(data)
        self.name = unpacker.get_str(SANITIZE_CC)
        self.arguments = unpacker.get_str(SANITIZE_CC)
        return True

    def pack(self) -> bytes:
        return pack_str(self.name) + \
            pack_str(self.arguments)