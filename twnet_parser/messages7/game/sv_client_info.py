# generated by scripts/generate_messages.py

from twnet_parser.pretty_print import PrettyPrint
from twnet_parser.packer import Unpacker
from twnet_parser.chunk_header import ChunkHeader
from twnet_parser.packer import SANITIZE_CC, pack_int, pack_str
from typing import Annotated, Literal, Optional
import twnet_parser.enum7 as enum7

class MsgSvClientInfo(PrettyPrint):
    def __init__(
            self,
            chunk_header: Optional[ChunkHeader] = None,
            client_id: int = 0,
            local: bool = False,
            team: int = enum7.Team.SPECTATORS.value,
            name: str = 'default',
            clan: str = 'default',
            country: int = 0,
            skin_part_names: Annotated[list[str], 6] = \
                ['', '', '', '', '', ''],
            use_custom_colors: Annotated[list[bool], 6] = \
                [False, False, False, False, False, False],
            skin_part_colors: Annotated[list[int], 6] = \
                [0, 0, 0, 0, 0, 0],
            silent: bool = False
    ) -> None:
        self.message_type: Literal['system', 'game'] = 'game'
        self.message_name: str = 'sv_client_info'
        self.system_message: bool = False
        self.message_id: int = 18
        if not chunk_header:
            chunk_header = ChunkHeader(version = '0.7')
        self.header: ChunkHeader = chunk_header

        self.client_id: int = client_id
        self.local: bool = local
        self.team: int = team
        self.name: str = name
        self.clan: str = clan
        self.country: int = country
        self.skin_part_names = skin_part_names
        self.use_custom_colors = use_custom_colors
        self.skin_part_colors = skin_part_colors
        self.silent: bool = silent

    def __iter__(self):
        yield 'message_type', self.message_type
        yield 'message_name', self.message_name
        yield 'system_message', self.system_message
        yield 'message_id', self.message_id
        yield 'header', dict(self.header)

        yield 'client_id', self.client_id
        yield 'local', self.local
        yield 'team', self.team
        yield 'name', self.name
        yield 'clan', self.clan
        yield 'country', self.country
        yield 'skin_part_names', self.skin_part_names
        yield 'use_custom_colors', self.use_custom_colors
        yield 'skin_part_colors', self.skin_part_colors
        yield 'silent', self.silent

    # first byte of data
    # has to be the first byte of the message payload
    # NOT the chunk header and NOT the message id
    def unpack(self, data: bytes) -> bool:
        unpacker = Unpacker(data)
        self.client_id = unpacker.get_int()
        self.local = unpacker.get_int() == 1
        self.team = unpacker.get_int() # enum TEAM
        self.name = unpacker.get_str(SANITIZE_CC)
        self.clan = unpacker.get_str(SANITIZE_CC)
        self.country = unpacker.get_int()
        for i in range(0, 6):
            self.skin_part_names[i] = unpacker.get_str(SANITIZE_CC)
        for i in range(0, 6):
            self.use_custom_colors[i] = unpacker.get_int() == 1
        for i in range(0, 6):
            self.skin_part_colors[i] = unpacker.get_int()
        self.silent = unpacker.get_int() == 1
        return True

    def pack(self) -> bytes:
        return pack_int(self.client_id) + \
            pack_int(self.local) + \
            pack_int(self.team) + \
            pack_str(self.name) + \
            pack_str(self.clan) + \
            pack_int(self.country) + \
            b''.join([pack_str(x) for x in self.skin_part_names]) + \
            b''.join([pack_int(x) for x in self.use_custom_colors]) + \
            b''.join([pack_int(x) for x in self.skin_part_colors]) + \
            pack_int(self.silent)