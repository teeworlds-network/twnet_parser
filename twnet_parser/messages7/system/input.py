# generated by scripts/generate_messages.py

from twnet_parser.pretty_print import PrettyPrint
from twnet_parser.packer import Unpacker
from twnet_parser.chunk_header import ChunkHeader
from twnet_parser.packer import pack_int
from typing import Literal, Optional
from twnet_parser.snap7.player_input import ObjPlayerInput

class MsgInput(PrettyPrint):
    def __init__(
            self,
            chunk_header: Optional[ChunkHeader] = None,
            ack_snapshot: int = 0,
            intended_tick: int = 0,
            input_size: int = 0,
            input: Optional[ObjPlayerInput] = None
    ) -> None:
        self.message_type: Literal['system', 'game'] = 'system'
        self.message_name: str = 'input'
        self.system_message: bool = True
        self.message_id: int = 20
        if not chunk_header:
            chunk_header = ChunkHeader(version = '0.7')
        self.header: ChunkHeader = chunk_header

        self.ack_snapshot: int = ack_snapshot
        self.intended_tick: int = intended_tick
        self.input_size: int = input_size
        if not input:
            input = ObjPlayerInput()
        self.input: ObjPlayerInput = input

    def __iter__(self):
        yield 'message_type', self.message_type
        yield 'message_name', self.message_name
        yield 'system_message', self.system_message
        yield 'message_id', self.message_id
        yield 'header', dict(self.header)

        yield 'ack_snapshot', self.ack_snapshot
        yield 'intended_tick', self.intended_tick
        yield 'input_size', self.input_size
        yield 'input', self.input.to_dict_payload_only()

    # first byte of data
    # has to be the first byte of the message payload
    # NOT the chunk header and NOT the message id
    def unpack(self, data: bytes) -> bool:
        unpacker = Unpacker(data)
        self.ack_snapshot = unpacker.get_int()
        self.intended_tick = unpacker.get_int()
        self.input_size = unpacker.get_int()
        self.input.unpack(unpacker)
        return True

    def pack(self) -> bytes:
        return pack_int(self.ack_snapshot) + \
            pack_int(self.intended_tick) + \
            pack_int(self.input_size) + \
            self.input.pack()