# generated by scripts/generate_messages.py

from typing import Any, Dict

from twnet_parser.pretty_print import PrettyPrint
from twnet_parser.packer import Unpacker
from twnet_parser.packer import pack_int

class ObjPlayerInfo(PrettyPrint):
    def __init__(
            self,
            player_flags: int = 0,
            score: int = 0,
            latency: int = 0
    ) -> None:
        self.item_name: str = 'obj.player_info'
        self.type_id: int = 11
        self.id: int = 0
        self.size: int = 3

        self.player_flags: int = player_flags
        self.score: int = score
        self.latency: int = latency

    def __iter__(self):
        yield 'item_name', self.item_name
        yield 'type_id', self.type_id
        yield 'id', self.id
        yield 'size', self.size

        yield 'player_flags', self.player_flags
        yield 'score', self.score
        yield 'latency', self.latency

    def to_dict_payload_only(self) -> Dict[str, Any]:
        return {
            'player_flags': self.player_flags,
            'score': self.score,
            'latency': self.latency
        }

    # first byte of data
    # has to be the first byte of the message payload
    # NOT the chunk header and NOT the message id
    def unpack(self, unpacker: Unpacker) -> bool:
        self.player_flags = unpacker.get_int() # TODO: this is a flag
        self.score = unpacker.get_int()
        self.latency = unpacker.get_int()
        return True

    def pack(self) -> bytes:
        return pack_int(self.player_flags) + \
            pack_int(self.score) + \
            pack_int(self.latency)