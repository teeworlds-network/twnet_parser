#!/bin/bash

RESET="\e[0m"
GREEN="\e[1;32m"
RED="\033[1;91m"
BG_RED="\033[0;101m"

# TODO: use yq to parse .gitlab-ci.yaml and auto run whatever the CI does

SCRIPT_ROOT="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 && pwd -P )"

cd "$SCRIPT_ROOT/.." || exit 1

err() {
	local msg="$1"
	printf '❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌\n'
	printf '%b%s%b%b%s%b\n' "$BG_RED" "[FATAL FAILURE]" "$RESET" "$RED" " Error: $msg" "$RESET"
	printf '❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌\n'
	exit 1
}

bash "$SCRIPT_ROOT/lint_json.sh" || err "json lint failed!"
find . -not -path './venv/*' -type f -name '*.sh' -print0 | xargs -0 shellcheck -x || err "shellcheck failed!"
pytest . || err "pytest failed!"
pylint twnet_parser/ || err "pylint failed!"
mypy twnet_parser/ || err "mypy failed!"
pyright twnet_parser/ || err "pyright failed!"
pyright scripts/generate_messages.py || err "pyright failed!"
if grep -r 'List\[' twnet_parser tests/
then
	err "found usage of List[ please use list[ instead"
fi

printf "\n%b%s%b\n" "$GREEN" "✅ all tests passed." "$RESET"

