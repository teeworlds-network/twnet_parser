#!/usr/bin/env bash

# credits go to Waku-2
# https://stackoverflow.com/a/46033999/21335246

previous_tag=0

printf "# Changelog\n\n"

for current_tag in $(git tag --sort=-creatordate)
do
	if [ "$previous_tag" != 0 ]
	then
		tag_date=$(git log -1 --pretty=format:'%ad' --date=short "$previous_tag")
		printf "## %s (%s)\n\n" "$previous_tag" "$tag_date"
		git log "${current_tag}...$previous_tag" --pretty=format:'*  %s [View](https://gitlab.com/teeworlds-network/twnet_parser/-/commit/%H)' --reverse | grep -v Merge
		printf "\n\n"
	fi
	[ "$previous_tag" != "0" ] && break
	previous_tag=${current_tag}
done
