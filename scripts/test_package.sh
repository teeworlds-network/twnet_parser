#!/bin/bash

if [ ! -f ./scripts/run_tests.sh ]
then
	echo "Error: ./scripts/run_tests.sh not found"
	exit 1
fi
if [ ! -f pyproject.toml ]
then
	echo "Error: pyproject.toml not found"
	exit 1
fi

echo "[*] wiping old dist ..."
[[ -d dist ]] && rm -rf dist

if ! python -m build
then
	echo "Error: build failed"
	exit 1
fi

if ! compgen -G ./dist/twnet_parser-*.tar.gz > /dev/null
then
	echo "Error: build did not generate expected file"
	echo "       dist/twnet_parser-*.tar.gz"
	exit 1
fi

if ! pip install dist/twnet_parser-*.tar.gz
then
	echo "Error: local test install of package failed"
	exit 1
fi
if ! python -c "import twnet_parser"
then
	echo "Error: local test import failed"
	exit 1
fi

rm -rf /tmp/twnet_parser_test
mkdir -p /tmp/twnet_parser_test
cp -r tests /tmp/twnet_parser_test || exit 1
cp dist/twnet_parser-*.tar.gz /tmp/twnet_parser_test/
(
	deactivate &>/dev/null
	cd /tmp/twnet_parser_test || exit 1
	python -m venv venv
	# shellcheck disable=SC1091
	source venv/bin/activate
	if ! pip install twnet_parser-*.tar.gz
	then
		echo "Error: local test install of package failed (venv)"
		exit 1
	fi
	if ! python -c "import twnet_parser"
	then
		echo "Error: local test import failed (venv)"
		exit 1
	fi
	if ! python -c "import twnet_parser.packet"
	then
		echo "Error: local test import twnet_parser.packet failed (venv)"
		exit 1
	fi
	pip install pytest
	python -m pytest tests || exit 1
) || {
	echo "[*] venv install tests failed.";
	exit 1;
}

echo "[*] venv tests passed."

#
# examples/print_pcap_files
#

pushd examples/06_and_07/print_pcap_files || exit 1
pip install dpkt || exit 1
./print_pcap_files.py 07_dm1.pcap || exit 1
if ! ./print_pcap_files.py 07_dm1.pcap | grep -q snap_empty
then
	echo "Error: examples/print_pcap_files.py did not print snap_empty"
	exit 1
fi
popd || exit 1

#
# examples/flood
#

pushd examples/07/flood || exit 1
./flood.py -h || exit 1
popd || exit 1
