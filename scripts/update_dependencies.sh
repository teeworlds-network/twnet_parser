#!/bin/bash

set -euo pipefail

tmp_venv=tmp-dep-venv
tmp_file=require.txt.tmp

if [ -d "$tmp_venv" ]
then
	echo "Error: please delete $tmp_venv first"
	exit 1
fi

python3 -m venv "$tmp_venv"

update_requirements() {
	local name="$1"
	# shellcheck disable=SC1091,SC1090
	source "$tmp_venv/bin/activate"
	pip install -r "requirements/root_${name}.txt"

	# find first actual requirement line
	# everything above might be manually edited
	# such as "-r prod.txt"
	# or comments
	local offset
	if ! offset="$(grep -nF '==' "requirements/${name}.txt" | head -n1 | cut -d':' -f1)"
	then
		echo "Error: failed to find offset. Is the requirements/${name}.txt file empty?"
		exit 1
	fi
	offset="$((offset-1))"

	{
		head -n"$offset" "requirements/${name}.txt"
		pip freeze
	} > "$tmp_file"
	mv "$tmp_file" "requirements/${name}.txt"
}

update_requirements dev

rm -rf "$tmp_venv"
