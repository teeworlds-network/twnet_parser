#!/bin/bash

printf 'Checking json '
for j in ./**/*.json;
do
	[[ -f "$j" ]] || continue

	if ! jq . "$j" &> /dev/null
	then
		echo -e "\nInvalid json file: $j";
		jq . "$j" || exit 1;
	else
		printf '.'
	fi
done
printf '\n'

