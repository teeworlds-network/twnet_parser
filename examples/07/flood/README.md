# flood.py

Fill server with tees. Please only run this on local servers or if you have the permission of the server owner.

```
pip install twnet_parser
```

And then you can run the example like this:

```
$ ./flood.py
usage: flood.py HOST PORT [NUM_TEES]
description:
  connects to given server
  and keeps the connection alive
example:
  flood.py localhost 8303 2
```

```
$ ./flood.py localhost 8303 2
[0] Connecting to localhost:8303 ...
[1] Connecting to localhost:8303 ...
[0] got msg 5
[1] got msg 5
[0] got msg 2
[1] got msg 2
[0] got msg 2
[1] got msg 2
[0] got msg 1
[0] got msg 17
[0] got msg 5
[0] sending info
[1] got msg 1
[1] got msg 17
[1] got msg 5
[1] sending info
[0] got msg 11
[0] got msg 6
[0] got msg 8
[1] got msg 11
[1] got msg 6
[1] got msg 8
[0] got msg 4
[1] got msg 4
[0] got msg 9
[0] got msg 18
[0] got msg 18
[0] got msg 8
[1] got msg 9
[1] got msg 18
[1] got msg 18
[1] got msg 8
[0] got msg 8
[1] got msg 8
[0] got msg 8
[1] got msg 8
[0] got msg 8
[1] got msg 8
^CSIGINT or CTRL-C detected. Exiting gracefully
[0] sending disconnect
[1] sending disconnect
```

